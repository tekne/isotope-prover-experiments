use anyhow::Error;
use atty::Stream;
use clap::{App, Arg};
use isotope_core_parser::utils::ws;
use log::*;
use nom::combinator::*;
use nom::sequence::*;
use rustyline::{error::ReadlineError, Editor};

use isotope_repl::*;

fn main() -> Result<(), Error> {
    // Handle command line arguments
    let matches = App::new("isotope-repl")
        .version("0.0.0")
        .author("Jad Ghalayini")
        .about("A REPL for interacting and experimenting with the isotope API")
        .arg(
            Arg::with_name("history")
                .short("h")
                .long("history")
                .value_name("FILE")
                .help("Sets a file to load history from")
                .takes_value(true),
        )
        .get_matches();

    let is_tty = atty::is(Stream::Stdout);
    if is_tty {
        println!("isotope-repl 0.0.0");
    }

    // Initialize logging
    pretty_env_logger::init_timed();

    // Initialize editor and helper
    let mut editor = Editor::<IsotopeHelper>::new();
    let helper = IsotopeHelper::new();

    let history = matches.value_of("history").unwrap_or(".isotope_history");

    if is_tty {
        if let Err(err) = editor.load_history(history) {
            info!("No history loaded from {}: {}", history, err)
        } else {
            info!("Successfully loaded history from {}", history)
        }
        editor.set_helper(Some(helper));
    }

    // Initialize repl state
    let mut repl = Repl::new(ReplConfig::default());

    // Read-Evaluate-Print Loop
    'repl: loop {
        let line = editor.readline(">> ");
        match line {
            Ok(line) => {
                let mut line = line.as_str();
                while let Ok((rest, (command_str, command))) =
                    preceded(opt(ws), consumed(command))(line)
                {
                    if let Err(err) = repl.handle(&command) {
                        //TODO: terminate if not a tty?
                        error!("Error handling command: {}", err);
                        if !is_tty {
                            break 'repl;
                        }
                        editor.add_history_entry(line);
                        break;
                    } else {
                        editor.add_history_entry(command_str);
                    }
                    line = rest;
                }
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break 'repl,
            Err(err) => {
                println!("{}", err);
                break;
            }
        }
    }

    if is_tty {
        editor.save_history(history)?;
    }

    Ok(())
}
