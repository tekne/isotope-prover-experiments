use isotope_core_parser::atom;
pub use super::*;

/// A command to the `isotope` rewpl
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    /// An `isotope` expression
    Expr(Expr),
    /// An `isotope` statement
    Stmt(Stmt),
    /// Get the code of an expression
    Code(Expr),
    /// Get the address of an expression
    Addr(Expr),
    /// Get the base type of an expression
    Base(Expr),
    /// Typecheck an expression
    Check(Expr),
    /// Join two expressions
    Join((Expr, Expr)),
}

/// Parse a command
pub fn command(input: &str) -> IResult<&str, Command> {
    alt((
        map(stmt, Command::Stmt),
        map(expr, Command::Expr),
        map(
            delimited(
                preceded(tag("#code"), ws),
                expr,
                delimited(opt(ws), tag(";"), opt(ws)),
            ),
            Command::Code,
        ),
        map(
            delimited(
                preceded(tag("#addr"), ws),
                expr,
                delimited(opt(ws), tag(";"), opt(ws)),
            ),
            Command::Addr,
        ),
        map(
            delimited(
                preceded(tag("#base"), ws),
                expr,
                delimited(opt(ws), tag(";"), opt(ws)),
            ),
            Command::Base,
        ),
        map(
            delimited(
                preceded(tag("#check"), ws),
                expr,
                delimited(opt(ws), tag(";"), opt(ws)),
            ),
            Command::Check,
        ),
        map(
            delimited(
                preceded(tag("#join"), ws),
                separated_pair(atom, ws, atom),
                delimited(opt(ws), tag(";"), opt(ws)),
            ),
            Command::Join,
        ),
    ))(input)
}
