/*!
Utility parsers
*/
use std::str::FromStr;

use super::*;

/// Parse whitespace between `isotope` identifiers
///
/// This includes both actual whitespace and comments
///
/// # Example
/// ```rust
/// # use isotope_core_parser::utils::ws;
/// assert_eq!(
///     ws("  /* multiline // comment */ // line comment \ndone").unwrap().0,
///     "done"
/// );
/// ```
pub fn ws(input: &str) -> IResult<&str, &str> {
    recognize(many1_count(alt((
        multispace1,
        line_comment,
        multiline_comment,
    ))))(input)
}

/// Parse a single line comment
///
/// Single-line comments begin with `//` and run to the end of the line, e.g.
/// ```text
/// // I'm a single line comment
/// ```
///
/// # Example
/// ```rust
/// # use isotope_core_parser::utils::line_comment;
/// assert_eq!(
///     line_comment("//line comment \n//hello"),
///     Ok(("\n//hello", "line comment "))
/// );
/// assert_eq!(
///     line_comment("// // nested\nhello"),
///     Ok(("\nhello", " // nested"))
/// )
/// ```
pub fn line_comment(input: &str) -> IResult<&str, &str> {
    preceded(tag("//"), not_line_ending)(input)
}

/// Parse a multi-line comment.
///
/// Multi-line comments are delimited by `/*` and `*/`, and may be nested, e.g.
/// ```text
/// /*
/// I'm a comment!
///     /*
///     I'm a nested comment!
///     */
/// */
/// ```
///
/// # Example
/// ```rust
/// # use isotope_core_parser::utils::multiline_comment;
/// assert_eq!(
///     multiline_comment("/* comment */hello"),
///     Ok(("hello", " comment "))
/// );
/// assert_eq!(
///     multiline_comment("/*/*nested*/*//*other*/"),
///     Ok(("/*other*/", "/*nested*/"))
/// );
/// assert_eq!(
///     multiline_comment(
///         "/* deeply /*/* nested */ and */ se /* parated */ */"
///     ),
///     Ok(("", " deeply /*/* nested */ and */ se /* parated */ "))
/// );
/// ```
pub fn multiline_comment(input: &str) -> IResult<&str, &str> {
    fn multiline_inner(input: &str) -> IResult<&str, ()> {
        let mut chars = input.chars();
        let mut rest = input;
        while let Some(c) = chars.next() {
            if c == '*' {
                if let Some('/') = chars.next() {
                    break;
                }
            }
            if c == '/' {
                if let Some('*') = chars.next() {
                    let (after_comment, _) = multiline_comment(rest)?;
                    chars = after_comment.chars();
                }
            }
            rest = chars.as_str();
        }
        Ok((rest, ()))
    }
    delimited(tag("/*"), recognize(multiline_inner), tag("*/"))(input)
}


/// Parse a string forming either a valid `isotope` identifier, or a hole ("`_`").
///
/// See: [`ident`]
pub fn opt_ident(input: &str) -> IResult<&str, Option<&str>> {
    alt((value(None, tag(HOLE)), map(ident, Some)))(input)
}


/// Parse a string forming a valid `isotope` identifier
///
/// An `isotope` identifier may be any sequence not containing [special characters](`SPECIAL_CHARACTERS`) (including whitespace) 
/// which is not a keyword (see [`is_keyword`]). 
/// 
/// This parser does *not* consume preceding whitespace!
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::utils::ident;
/// assert_eq!(ident("hello "), Ok((" ", "hello")));
/// assert!(ident(" bye").is_err());
/// assert!(ident("0x35").is_err());
/// assert_eq!(ident("x35"), Ok(("", "x35")));
/// assert_eq!(ident("你好"), Ok(("", "你好")));
/// let arabic = ident("الحروف العربية").unwrap();
/// let desired_arabic = (" العربية" ,"الحروف");
/// assert_eq!(arabic, desired_arabic);
/// ```
pub fn ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARACTERS), |ident: &str| !is_keyword(ident))(input)
}



/// An integer type which can be parsed from a string
pub trait IntFromStr: Sized {
    /// Parse a type from a string given a radix
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()>;
}

impl IntFromStr for u8 {
    #[inline]
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()> {
        Self::from_str_radix(src, radix).map_err(|_| ())
    }
}

impl IntFromStr for u16 {
    #[inline]
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()> {
        Self::from_str_radix(src, radix).map_err(|_| ())
    }
}

impl IntFromStr for u32 {
    #[inline]
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()> {
        Self::from_str_radix(src, radix).map_err(|_| ())
    }
}

impl IntFromStr for u64 {
    #[inline]
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()> {
        Self::from_str_radix(src, radix).map_err(|_| ())
    }
}

impl IntFromStr for u128 {
    #[inline]
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()> {
        Self::from_str_radix(src, radix).map_err(|_| ())
    }
}

impl IntFromStr for BigInt {
    #[inline]
    fn parse_radix(src: &str, radix: u32) -> Result<Self, ()> {
        Self::parse_bytes(src.as_bytes(), radix).ok_or(())
    }
}

/// Parse a natural number
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::utils::natural;
/// assert_eq!(natural::<u32>("5"), Ok(("", 5)));
/// assert_eq!(natural::<u32>("0x5"), Ok(("", 5)));
/// assert_eq!(natural::<u32>("0xfa"), Ok(("", 0xfa)));
/// assert_eq!(natural::<u32>("0o15"), Ok(("", 0o15)));
/// assert_eq!(natural::<u32>("0b10"), Ok(("", 0b10)));
/// // Only allows valid digits:
/// assert_eq!(natural::<u32>("0b20"), Ok(("b20", 0)));
/// assert_eq!(natural::<u32>("0b02"), Ok(("2", 0)));
/// // Overflow!
/// assert!(natural::<u32>("10000000000").is_err());
/// assert_eq!(natural::<u64>("10000000000"), Ok(("", 10000000000)));
/// assert!(natural::<u64>("100000000000000000000").is_err());
/// ```
pub fn natural<F: IntFromStr>(input: &str) -> IResult<&str, F> {
    alt((
        map_res(preceded(tag("0x"), hex_digit1), |src| {
            F::parse_radix(src, 16)
        }),
        map_res(preceded(tag("0o"), oct_digit1), |src| {
            F::parse_radix(src, 8)
        }),
        map_res(preceded(tag("0b"), is_a("01")), |src| {
            F::parse_radix(src, 2)
        }),
        map_res(digit1, |src| F::parse_radix(src, 10)),
    ))(input)
}

/// Parse a decimal integer literal
///
/// # Examples
/// ```rust
/// # use isotope_core_parser::utils::decimal;
/// assert_eq!(decimal::<u32>("5"), Ok(("", 5)));
/// // Does not recognize hexadecimal literals, etc!
/// assert_eq!(decimal::<u32>("0x5"), Ok(("x5", 0)));
/// // Overflow!
/// assert!(decimal::<u32>("10000000000").is_err());
/// assert_eq!(decimal::<u64>("10000000000"), Ok(("", 10000000000)));
/// assert!(decimal::<u64>("100000000000000000000").is_err());
/// ```
pub fn decimal<F: FromStr>(input: &str) -> IResult<&str, F> {
    map_res(digit1, str::parse)(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn line_comment_works() {
        let comment_examples = [
            "// I'm a line comment",
            "// I'm a line comment, with /* a non-nested comment */",
            "// /* I'm a // line comment with a /* nested */ multiline (and line) comment */",
        ];
        for s in comment_examples.iter().copied() {
            assert_eq!(line_comment(s).unwrap().0, "")
        }
    }

    #[test]
    fn multiline_comment_works() {
        let comment_examples = [
            "/* Hello, I'm a /* nested /* comment */ */ */",
            "/* I'm a non-nested comment */",
            "/* I'm a // line comment in a /* nested */ multiline comment */",
        ];
        for s in comment_examples.iter().copied() {
            assert_eq!(multiline_comment(s).unwrap().0, "")
        }
    }

    #[test]
    fn decimal_works() {
        let full_examples = [
            ("53", 53),
            ("75", 75),
            ("456", 456),
            ("352", 352),
            ("98", 98),
        ];
        for (s, n) in full_examples.iter().copied() {
            if n <= u8::MAX as u32 {
                assert_eq!(decimal(s), Ok(("", n as u8)));
            }
            assert_eq!(decimal(s), Ok(("", n as u16)));
            assert_eq!(decimal(s), Ok(("", n as u32)));
            assert_eq!(decimal(s), Ok(("", n as u64)));
            assert_eq!(decimal(s), Ok(("", n as u128)));
        }
    }

    #[test]
    fn natural_works() {
        let full_examples = [
            ("53", 53),
            ("75", 75),
            ("456", 456),
            ("352", 352),
            ("98", 98),
            ("0x4ff4", 0x4ff4),
            ("0b00111011", 0b00111011),
            ("0o5113", 0o5113),
        ];
        for (s, n) in full_examples.iter().copied() {
            if n <= u8::MAX as u32 {
                assert_eq!(natural(s), Ok(("", n as u8)));
            }
            assert_eq!(natural(s), Ok(("", n as u16)));
            assert_eq!(natural(s), Ok(("", n as u32)));
            assert_eq!(natural(s), Ok(("", n as u64)));
            assert_eq!(natural(s), Ok(("", n as u128)));
        }
    }
}
