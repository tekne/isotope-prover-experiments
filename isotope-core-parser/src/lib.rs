/*!
A `nom`-based parser for the core `isotope` language
*/
#![forbid(missing_debug_implementations, missing_docs, unsafe_code)]

use ast::*;
pub use isotope_core_ast as ast;

pub use ast::{Arc, BigInt, SmallVec, SmolStr};
pub use nom::IResult;

use nom::branch::*;
use nom::bytes::complete::*;
use nom::character::complete::*;
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use smallvec::smallvec;

pub mod token;
pub mod utils;

use token::*;
use utils::*;

/// Parse an `isotope` expression
//TODO: result type annotation?
pub fn expr(input: &str) -> IResult<&str, Expr> {
    let (mut rest, mut curr) = atom(input)?;
    let mut app = SmallVec::new();
    let pi = loop {
        match preceded(opt(ws), alt((map(atom, Some), value(None, tag(ARROW)))))(rest) {
            Ok((rem, Some(arg))) => {
                rest = rem;
                app.push(Arc::new(curr));
                curr = arg;
            }
            Ok((rem, None)) => {
                rest = rem;
                break true;
            }
            Err(nom::Err::Incomplete(n)) => return Err(nom::Err::Incomplete(n)),
            Err(_) => break false,
        };
    };

    let is_app = !app.is_empty();
    if is_app {
        app.push(Arc::new(curr));
        curr = Expr::App(App(app));
    }

    if pi {
        let (rest, result) = preceded(opt(ws), expr)(rest)?;
        //TODO: multiple arguments
        match curr {
            Expr::Annotated(Annotated { expr, ty }) if matches!(*expr, Expr::Ident(_)) => {
                debug_assert!(!is_app);
                //TODO: optimize, can remove allocation here
                let param_name = if let Expr::Ident(id) = &*expr {
                    id.clone()
                } else {
                    unreachable!()
                };
                Ok((
                    rest,
                    Expr::Pi(Pi {
                        param_name,
                        param_ty: ty,
                        result: Arc::new(result),
                    }),
                ))
            }
            curr => Ok((
                rest,
                Expr::Pi(Pi {
                    param_name: None,
                    param_ty: Arc::new(curr),
                    result: Arc::new(result),
                }),
            )),
        }
    } else {
        Ok((rest, curr))
    }
}

/// Parse an (atomic) `isotope` expression
pub fn atom(input: &str) -> IResult<&str, Expr> {
    alt((
        sexpr,
        map(scope, Expr::Scope),
        map(lambda, Expr::Lambda),
        map(universe, Expr::Universe),
        map(case, Expr::Case),
        map(natural, Expr::Int),
        map(opt_ident, |ident| Expr::Ident(ident.map(SmolStr::new))),
    ))(input)
}

/// Parse an S-expression wrapped in parentheses, with an optional annotation
pub fn sexpr(input: &str) -> IResult<&str, Expr> {
    delimited(preceded(tag("("), opt(ws)), sexpr_inner, preceded(opt(ws), tag(")")))(input)
}

/// Parse the inside of an S-expression wrapped in parentheses, with an optional annotation
pub fn sexpr_inner(input: &str) -> IResult<&str, Expr> {
    map_opt(
        pair(
            opt(expr),
            opt(delimited(
                delimited(opt(ws), tag(":"), opt(ws)),
                expr,
                opt(ws),
            )),
        ),
        |(expr, ty)| match (expr, ty) {
            (expr, None) => Some(expr.unwrap_or(Expr::App(App(SmallVec::new())))),
            (Some(expr), Some(ty)) => Some(Expr::Annotated(Annotated {
                expr: Arc::new(expr),
                ty: Arc::new(ty),
            })),
            _ => None,
        },
    )(input)
}

/// Parse a lambda function
//TODO: result type annotation?
pub fn lambda(input: &str) -> IResult<&str, Lambda> {
    map_opt(
        separated_pair(
            delimited(
                preceded(alt((tag(LAMBDA), tag(SIMPLE_LAMBDA))), opt(ws)),
                sexpr_inner,
                opt(ws),
            ),
            tag(MAPSTO),
            preceded(opt(ws), expr),
        ),
        |(param, result)| {
            //TODO: multiple arguments
            let (param_name, param_ty) = match param {
                Expr::Ident(param_name) => (param_name, None),
                Expr::Annotated(Annotated { expr, ty }) if matches!(&*expr, Expr::Ident(_)) => {
                    //TODO: optimize, can remove allocation here
                    if let Expr::Ident(param_name) = &*expr {
                        (param_name.clone(), Some(ty))
                    } else {
                        unreachable!()
                    }
                }
                _ => return None,
            };
            Some(Lambda {
                param_name,
                param_ty,
                result: Arc::new(result),
            })
        },
    )(input)
}

/// Parse a typing universe
pub fn universe(input: &str) -> IResult<&str, Universe> {
    preceded(tag("#U"), map(decimal, Universe))(input)
}

/// Parse a case statement
pub fn case(input: &str) -> IResult<&str, Case> {
    preceded(
        tag("#case"),
        preceded(
            ws,
            map(separated_pair(atom, ws, atom), |(family, target)| Case {
                family: Arc::new(family),
                target: Arc::new(target),
            }),
        ),
    )(input)
}

/// Parse a scope
pub fn scope(input: &str) -> IResult<&str, Scope> {
    delimited(
        preceded(tag("{"), opt(ws)),
        scope_inner,
        preceded(opt(ws), tag("}")),
    )(input)
}

/// Parse a recursive definition
pub fn recursive(input: &str) -> IResult<&str, Recursive> {
    alt((
        map(
            delimited(
                tuple((tag("#rec"), opt(ws), tag("{"), opt(ws))),
                separated_list0(opt(ws), let_),
                preceded(opt(ws), tag("}")),
            ),
            |defs| Recursive {
                defs: SmallVec::from(defs),
            },
        ),
        map(preceded(preceded(tag("#letrec"), ws), let_inner), |let_| {
            Recursive {
                defs: smallvec![let_],
            }
        }),
    ))(input)
}

/// Parse an inner scope
pub fn scope_inner(input: &str) -> IResult<&str, Scope> {
    //TODO: optimize
    map(
        separated_pair(separated_list0(opt(ws), map(stmt, Arc::new)), opt(ws), expr),
        |(stmts, retv)| Scope {
            stmts: SmallVec::from(stmts),
            retv: Arc::new(retv),
        },
    )(input)
}

/// Parse a statement
pub fn stmt(input: &str) -> IResult<&str, Stmt> {
    alt((
        map(let_, Stmt::Let),
        map(inductive, Stmt::Inductive),
        map(recursive, Stmt::Recursive),
    ))(input)
}

/// Parse a let-statement
pub fn let_(input: &str) -> IResult<&str, Let> {
    preceded(preceded(tag("#let"), ws), let_inner)(input)
}

/// Parse an inner let-statement
pub fn let_inner(input: &str) -> IResult<&str, Let> {
    map(
        tuple((
            opt_ident,
            opt(ws),
            opt(preceded(
                preceded(tag(":"), opt(ws)),
                terminated(expr, opt(ws)),
            )),
            tag("="),
            opt(ws),
            expr,
            opt(ws),
            tag(";"),
        )),
        |(ident, _ws1, ty, _eq, _ws2, value, _ws3, _sc)| Let {
            ident: ident.map(SmolStr::new),
            ty: ty.map(Arc::new),
            value: Arc::new(value),
        },
    )(input)
}

/// Parse an inductive definition
pub fn inductive(input: &str) -> IResult<&str, Inductive> {
    delimited(
        preceded(tag("#data"), ws),
        map(many1(preceded(opt(ws), inductive_family)), |families| {
            Inductive {
                families: families.into(),
            }
        }),
        tag(";"),
    )(input)
}

/// Parse an inductive family
pub fn inductive_family(input: &str) -> IResult<&str, InductiveFamily> {
    map(
        tuple((
            terminated(ident, opt(ws)),
            opt(delimited(preceded(tag(":"), opt(ws)), expr, opt(ws))),
            delimited(
                preceded(tag("{"), opt(ws)),
                separated_list0(
                    delimited(opt(ws), tag(";"), opt(ws)),
                    separated_pair(
                        map(ident, SmolStr::new),
                        delimited(opt(ws), tag(":"), opt(ws)),
                        map(expr, Arc::new),
                    ),
                ),
                delimited(
                    preceded(opt(ws), opt(preceded(tag(";"), opt(ws)))),
                    tag("}"),
                    opt(ws),
                ),
            ),
        )),
        |(name, ty, cons)| InductiveFamily {
            name: SmolStr::new(name),
            ty: ty.map(Arc::new),
            cons,
        },
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;
    use smallvec::smallvec;

    #[test]
    fn expr_parsing() {
        let f = Arc::new(Expr::Ident(Some("f".into())));
        let x = Arc::new(Expr::Ident(Some("x".into())));
        let f_x = Expr::App(App(smallvec![f.clone(), x.clone()]));
        let f_x_arc = Arc::new(f_x.clone());
        let id = Expr::Lambda(Lambda {
            param_name: Some("x".into()),
            param_ty: None,
            result: x,
        });
        let id_arc = Arc::new(id.clone());
        let examples = [
            ("f x", f_x.clone()),
            ("(f x)", f_x.clone()),
            ("f\nx", f_x.clone()),
            ("(\nf\nx\n)", f_x),
            (
                "(f x) (f x)",
                Expr::App(App(smallvec![f_x_arc.clone(), f_x_arc.clone()])),
            ),
            (
                "(f x) (f x)",
                Expr::App(App(smallvec![f_x_arc.clone(), f_x_arc.clone()])),
            ),
            ("λx => x", id),
            (
                "(λx => x)(λx => x)",
                Expr::App(App(smallvec![id_arc.clone(), id_arc.clone()])),
            ),
        ];

        for (input, target) in examples {
            let (rest, output) = expr(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, target)
        }
    }

    #[test]
    fn expr_arrow_remainder() {
        assert_eq!(expr("x =>"), Ok((" =>", Expr::Ident(Some("x".into())))))
    }

    #[test]
    fn ident_parsing() {
        assert_eq!(ident("x"), Ok(("", "x")));
        assert_eq!(expr("x"), Ok(("", Expr::Ident(Some("x".into())))));
        assert!(ident("#true").is_err());
    }

    #[test]
    fn lambda_parsing() {
        let examples = [
            (
                "λx => x",
                Lambda {
                    param_name: Some("x".into()),
                    param_ty: None,
                    result: Expr::Ident(Some("x".into())).into(),
                },
            ),
            (
                "#lam x => x",
                Lambda {
                    param_name: Some("x".into()),
                    param_ty: None,
                    result: Expr::Ident(Some("x".into())).into(),
                },
            ),
            (
                "λ_ => x",
                Lambda {
                    param_name: None,
                    param_ty: None,
                    result: Expr::Ident(Some("x".into())).into(),
                },
            ),
            (
                "#lam _ => x",
                Lambda {
                    param_name: None,
                    param_ty: None,
                    result: Expr::Ident(Some("x".into())).into(),
                },
            ),
            (
                "#lam x: nat => true",
                Lambda {
                    param_name: Some("x".into()),
                    param_ty: Some(Expr::Ident(Some("nat".into())).into()),
                    result: Expr::Ident(Some("true".into())).into(),
                },
            ),
            (
                "#lam _: bool => true",
                Lambda {
                    param_name: None,
                    param_ty: Some(Expr::Ident(Some("bool".into())).into()),
                    result: Expr::Ident(Some("true".into())).into(),
                },
            ),
        ];
        for (input, target) in &examples {
            let (rest, output) = lambda(input).expect(input);
            assert_eq!(rest, "");
            assert_eq!(output, *target);
            let (rest, output) = expr(input).expect(input);
            assert_eq!(rest, "");
            assert_eq!(output, Expr::Lambda(target.clone()));
        }
    }

    #[test]
    fn arrow_parsing() {
        let a_to_a = Expr::Pi(Pi {
            param_name: None,
            param_ty: Expr::Ident(Some("A".into())).into(),
            result: Expr::Ident(Some("A".into())).into(),
        });
        let arc_a_to_a = Arc::new(a_to_a.clone());
        let examples = [
            ("A -> A", a_to_a),
            (
                "X -> A -> A",
                Expr::Pi(Pi {
                    param_name: None,
                    param_ty: Expr::Ident(Some("X".into())).into(),
                    result: arc_a_to_a.clone(),
                }),
            ),
            (
                "(A -> A) -> X",
                Expr::Pi(Pi {
                    param_name: None,
                    param_ty: arc_a_to_a,
                    result: Expr::Ident(Some("X".into())).into(),
                }),
            ),
            (
                "(_: A) -> A",
                Expr::Pi(Pi {
                    param_name: None,
                    param_ty: Expr::Ident(Some("A".into())).into(),
                    result: Expr::Ident(Some("A".into())).into(),
                }),
            ),
            (
                "(x: A) -> A",
                Expr::Pi(Pi {
                    param_name: Some("x".into()),
                    param_ty: Expr::Ident(Some("A".into())).into(),
                    result: Expr::Ident(Some("A".into())).into(),
                }),
            ),
        ];
        for (input, target) in &examples {
            let (rest, output) = expr(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, *target);
        }
    }

    #[test]
    fn nats_parsing() {
        let nats_id = SmolStr::new("ℕ");
        let nats_ty = Arc::new(Expr::Ident(Some(nats_id.clone())));
        let nats = Stmt::Inductive(Inductive {
            families: smallvec![InductiveFamily {
                name: nats_id.clone(),
                ty: Some(Expr::Universe(Universe(0)).into()),
                cons: vec![
                    ("zero".into(), nats_ty.clone()),
                    (
                        "succ".into(),
                        Expr::Pi(Pi {
                            param_name: None,
                            param_ty: nats_ty.clone(),
                            result: nats_ty.clone()
                        })
                        .into()
                    )
                ]
            }],
        });
        assert_eq!(
            stmt("#data ℕ: #U0 { zero: ℕ; succ: ℕ -> ℕ; };").unwrap().1,
            nats
        );
        assert_eq!(
            stmt("#data ℕ: #U0 { zero: ℕ; succ: ℕ -> ℕ };").unwrap().1,
            nats
        );
    }
}
