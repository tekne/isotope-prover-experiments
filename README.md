# The Isotope Project
`isotope` is an experimental dependently typed language supporting borrow
checking, designed for use as an intermediate representation for building
verifiable systems languages.

`isotope` is composed of two languages: the "main" or "implementation" language,
and a "term" language. Expressions in the "main" language describe dependently
typed [RVSDG]s extended with lifetimes, which are given denotations in the 
"core" or "term" language, which ignores low-level details such as lifetimes and
representation. In turn, the types of these expressions may depend only on
values in the "term" language, with programs, representations, and "main"
language types themselves values like any other. The "term" language itself is a
simplified implementation of the [Calculus of Constructions], inspired by
[Lean], extended with [Zombie]-like support for congruence-based normalization.

[RVSDG]: https://arxiv.org/abs/1912.05036
[Calculus of Constructions]: https://core.ac.uk/download/pdf/82038778.pdf
[Lean]: https://leanprover.github.io/
[Zombie]: https://www.seas.upenn.edu/~sweirich/papers/congruence-extended.pdf
