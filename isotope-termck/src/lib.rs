/*!
A termination checker for simple functional programs, based off [foetus](http://www2.tcs.ifi.lmu.de/~abel/foetus.pdf) and designed for use in `isotope-core`
*/
#![forbid(missing_debug_implementations, missing_docs, unsafe_code)]

use either::Either;
use indexmap::IndexSet;
use ndarray::{Array2, Axis};
use petgraph::{
    matrix_graph::MatrixGraph,
    prelude::{DiGraph, NodeIndex},
};
use smallvec::SmallVec;
use std::fmt::{self, Debug, Formatter};
use std::{cmp::Ordering, collections::VecDeque};

mod call_mat;
mod decreasing_args;
mod relation;

pub use call_mat::*;
pub use decreasing_args::*;
pub use relation::*;

/// A definition graph for a set of terminating mutually recursive definitions, annotated with those definitions' decreasing arguments
///
/// If a set of definitions failed the termination check, then those definitions' decreasing arguments will be `None`
#[derive(Debug, Clone)]
pub struct DefinitionGraph {
    /// The definitions in this graph
    pub definitions: DiGraph<Option<DecreasingArgs>, ()>,
    /// The component mapping of this graph, stored as node index, definition index pairs
    pub component_map: Vec<(usize, usize)>,
}

impl DefinitionGraph {
    /// Get the number of definitions in this graph
    #[inline]
    pub fn no_defs(&self) -> usize {
        self.component_map.len()
    }

    /// Get the number of components in this graph
    #[inline]
    pub fn no_components(&self) -> usize {
        self.definitions.node_count()
    }

    /// Get the decreasing arguments for a component, or `Some(None)` if that component is nonterminating
    #[inline]
    pub fn component_decreasing_args(&self, component: usize) -> Option<Option<&DecreasingArgs>> {
        self.definitions
            .node_weight(NodeIndex::new(component))
            .map(Option::as_ref)
    }

    /// Get the decreasing arguments for a definition, or `Some(None)` if that component is nonterminating
    #[inline]
    pub fn decreasing_args(&self, def_ix: usize) -> Option<Option<&[usize]>> {
        let (component, ix) = self.component(def_ix)?;
        let decreasing_args = self.component_decreasing_args(component);
        debug_assert_ne!(decreasing_args, None);
        if let Some(Some(decreasing_args)) = decreasing_args {
            let args = decreasing_args.get(ix);
            debug_assert_ne!(args, None);
            Some(args)
        } else {
            Some(None)
        }
    }

    /// Get the component of a definition in this graph
    #[inline]
    pub fn component(&self, def_ix: usize) -> Option<(usize, usize)> {
        self.component_map.get(def_ix).copied()
    }
}

/// A termination order for a function
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TerminationOrder(pub SmallVec<[usize; 2]>);

/// Recursion behaviour for a function
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct RecursionBehaviour(pub Array2<Relation>);

impl RecursionBehaviour {
    /// Check whether this recursion behaviour is equivalent to another
    ///
    /// Note that this is currently quite slow, and intended mainly for testing purposes.
    pub fn equiv(&self, other: &RecursionBehaviour) -> bool {
        //TODO: optimize?
        if self.0.shape() != other.0.shape() {
            return false;
        }
        let no_calls = self.0.shape()[0];
        let mut permutations = Vec::with_capacity(no_calls * 2);
        permutations.extend(0..no_calls);
        permutations.extend(0..no_calls);
        let (this_permutation, other_permutation) = permutations.split_at_mut(no_calls);

        fn sort_permutation_by(this: &RecursionBehaviour, left: usize, right: usize) -> Ordering {
            this.0
                .index_axis(Axis(0), left)
                .iter()
                .map(|rel| *rel as u8)
                .cmp(
                    this.0
                        .index_axis(Axis(0), right)
                        .iter()
                        .map(|rel| *rel as u8),
                )
        }

        this_permutation.sort_unstable_by(|left, right| sort_permutation_by(self, *left, *right));
        other_permutation.sort_unstable_by(|left, right| sort_permutation_by(other, *left, *right));

        for (&this_ix, &other_ix) in this_permutation.iter().zip(other_permutation.iter()) {
            if self.0.index_axis(Axis(0), this_ix) != other.0.index_axis(Axis(0), other_ix) {
                return false;
            }
        }
        true
    }

    /// Check whether a permutation is a termination order on it's elements
    ///
    /// This corresponds to Definition 1 from Section 5 of the [foetus](http://www2.tcs.ifi.lmu.de/~abel/foetus.pdf) paper.
    pub fn is_termination_order(&self, pi: &[usize]) -> bool {
        let no_args = self.0.shape()[1];
        'outer: for call in 0..self.0.shape()[0] {
            for &arg in pi {
                if arg >= no_args {
                    return false;
                }
                match self.0[(call, arg)] {
                    Relation::Equal => continue,
                    Relation::Less => continue 'outer,
                    _ => return false,
                }
            }
            return false;
        }
        true
    }

    /// Compute whether this recursion behaviour has a termination order
    #[inline]
    pub fn has_termination_order(&self) -> bool {
        self.termination_order().is_some()
    }

    /// Iterate over the decreasing arguments for this recursion behaviour in sorted order
    #[inline]
    pub fn decreasing_args(&self) -> impl Iterator<Item = usize> + '_ {
        (0..self.0.shape()[1]).filter(|arg| self.is_decreasing_arg(*arg))
    }

    /// Compute a termination order on this recursion behaviour. Returns `None` if and only if a termination order does not exist.
    pub fn termination_order(&self) -> Option<TerminationOrder> {
        let no_calls = self.0.shape()[0];
        let no_args = self.0.shape()[1];
        let mut candidate = Vec::with_capacity(no_args);
        let mut args = (0..(no_args)).collect();
        let mut calls = (0..(no_calls)).collect();
        let mut filtered = Vec::with_capacity(no_calls);
        if self.termination_order_helper(&mut candidate, &mut calls, &mut args, &mut filtered) {
            debug_assert!(self.is_termination_order(&candidate[..]));
            Some(TerminationOrder(candidate.into()))
        } else {
            None
        }
    }

    /// Compute a termination order on these elements. Returns an error if and only if a termination order does not exist.
    fn termination_order_helper(
        &self,
        candidate: &mut Vec<usize>,
        calls: &mut VecDeque<usize>,
        args: &mut VecDeque<usize>,
        filtered: &mut Vec<usize>,
    ) -> bool {
        //TODO: optimize this... a lot!
        if calls.len() == 0 || args.is_empty() {
            return true;
        }
        for _ in 0..args.len() {
            let arg = args.pop_front().unwrap();
            if let Some(pre_filtered) = self.filter_arg(arg, calls, filtered) {
                candidate.push(arg);
                if self.termination_order_helper(candidate, calls, args, filtered) {
                    return true;
                }
                candidate.pop();
                while filtered.len() != pre_filtered {
                    calls.push_back(filtered.pop().unwrap())
                }
            }
            args.push_back(arg);
        }
        false
    }

    /// Check whether a given argument is a decreasing argument
    #[inline]
    pub fn is_decreasing_arg(&self, arg: usize) -> bool {
        let mut has_lt = false;
        for call in 0..self.0.shape()[0] {
            match self.0[(call, arg)] {
                Relation::Equal => {}
                Relation::Less => has_lt = true,
                _ => return false,
            }
        }
        has_lt
    }

    /// If a given argument is a decreasing argument, filter calls, returning the old length
    fn filter_arg(
        &self,
        arg: usize,
        calls: &mut VecDeque<usize>,
        filtered: &mut Vec<usize>,
    ) -> Option<usize> {
        let pre_filtered = filtered.len();
        let mut has_unknown = false;
        for _ in 0..calls.len() {
            let call = calls.pop_front().unwrap();
            match self.0[(call, arg)] {
                Relation::Less => filtered.push(call),
                Relation::Equal => calls.push_back(call),
                Relation::Unknown => {
                    has_unknown = true;
                    break;
                }
            }
        }
        if has_unknown {
            while filtered.len() != pre_filtered {
                calls.push_back(filtered.pop().unwrap())
            }
        }
        if filtered.len() != pre_filtered {
            Some(pre_filtered)
        } else {
            None
        }
    }
}

/// A dense call graph
#[derive(Clone)]
pub struct DenseCallGraph {
    calls: MatrixGraph<(), IndexSet<CallMat>>,
}

impl DenseCallGraph {
    /// Create a new call-graph with the given node capacity
    pub fn with_capacity(nodes: usize) -> DenseCallGraph {
        DenseCallGraph {
            calls: MatrixGraph::with_capacity(nodes),
        }
    }

    /// Create a new call-graph with the given number of nodes
    pub fn with_nodes(nodes: usize) -> DenseCallGraph {
        let mut graph = DenseCallGraph::with_capacity(nodes);
        for _ in 0..nodes {
            graph.add_node()
        }
        graph
    }

    /// Add an edge to this call-graph
    pub fn add_edge(&mut self, caller: usize, callee: usize, call: CallMat) -> bool {
        let caller_ix = NodeIndex::new(caller);
        let callee_ix = NodeIndex::new(callee);
        if !self.calls.has_edge(caller_ix, callee_ix) {
            self.calls.add_edge(caller_ix, callee_ix, {
                let mut result = IndexSet::with_capacity(1);
                result.insert(call);
                result
            });
            true
        } else {
            self.calls
                .edge_weight_mut(caller_ix, callee_ix)
                .insert(call)
        }
    }

    /// Iterate over the edges between the caller and callee in this call-graph
    pub fn edges(
        &self,
        caller: usize,
        callee: usize,
    ) -> impl Iterator<Item = &CallMat> + ExactSizeIterator + Clone + '_ {
        let caller_ix = NodeIndex::new(caller);
        let callee_ix = NodeIndex::new(callee);
        if self.calls.has_edge(caller_ix, callee_ix) {
            Either::Left(self.calls.edge_weight(caller_ix, callee_ix).iter())
        } else {
            Either::Right(std::iter::empty())
        }
    }

    /// Count the number of times a caller calls the callee
    pub fn no_calls(&self, caller: usize, callee: usize) -> usize {
        let caller_ix = NodeIndex::new(caller);
        let callee_ix = NodeIndex::new(callee);
        if self.calls.has_edge(caller_ix, callee_ix) {
            self.calls.edge_weight(caller_ix, callee_ix).len()
        } else {
            0
        }
    }

    /// Get an indexed call
    fn get_call(&self, caller: usize, callee: usize, call_ix: usize) -> &CallMat {
        self.calls
            .edge_weight(NodeIndex::new(caller), NodeIndex::new(callee))
            .get_index(call_ix)
            .unwrap()
    }

    /// Add a node to this call-graph
    pub fn add_node(&mut self) {
        self.calls.add_node(());
    }

    /// Complete this call-graph
    pub fn complete(&mut self) {
        let node_count = self.calls.node_count();
        let mut originals = ndarray::Array::from_elem((node_count, node_count), (0, 0));
        for f in 0..node_count {
            for g in 0..node_count {
                originals[(f, g)].0 = self.no_calls(f, g)
            }
        }
        loop {
            let mut changed = false;

            //TODO: optimize by using an update set?
            for f in 0..node_count {
                for g in 0..node_count {
                    let fg_calls = self.no_calls(f, g);
                    for c_fg in originals[(f, g)].1..fg_calls {
                        for h in 0..node_count {
                            for c_gh in 0..originals[(g, h)].0 {
                                let new_call =
                                    self.get_call(f, g, c_fg) * self.get_call(g, h, c_gh);
                                changed |= self.add_edge(f, h, new_call);
                            }
                        }
                    }
                    originals[(f, g)].1 = fg_calls;
                }
            }

            if !changed {
                break;
            }
        }
    }

    /// Compute this call-graph's direct recursion behaviour matrix for a given definition
    ///
    /// Corresponds to actual recursion behaviour if the graph is complete
    pub fn direct_def_recursion_behaviour(&self, def_ix: usize) -> Option<RecursionBehaviour> {
        //TODO: prune unnecessary arguments *before* allocating a matrix

        if def_ix >= self.calls.node_count() {
            return None;
        }
        let no_calls = self.no_calls(def_ix, def_ix);
        let no_args = if no_calls == 0 {
            0
        } else {
            self.get_call(def_ix, def_ix, 0).shape().0
        };
        let mut result = Array2::zeros((no_calls, no_args));

        for c in 0..no_calls {
            for (i, d) in self.get_call(def_ix, def_ix, c).diag_iter().enumerate() {
                result[(c, i)] = d;
            }
        }

        Some(RecursionBehaviour(result))
    }

    /// Compute this call-graph's direct recursion behaviour matrix
    ///
    /// Corresponds to actual recursion behaviour if the graph is complete
    pub fn direct_recursion_behaviour(
        &self,
    ) -> impl Iterator<Item = RecursionBehaviour> + ExactSizeIterator + Clone + '_ {
        (0..self.calls.node_count())
            .map(|def_ix| self.direct_def_recursion_behaviour(def_ix).unwrap())
    }
}

impl Debug for DenseCallGraph {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        //TODO: implement
        f.debug_struct("DenseCallGraph")
            .field("no_defs", &self.calls.node_count())
            .field("no_condensed_edges", &self.calls.edge_count())
            .finish()
    }
}

/// A call graph
#[derive(Debug, Clone)]
pub struct CallGraph {
    calls: DiGraph<(), CallMat>,
}

impl CallGraph {
    /// Create a new call-graph with the given capacity
    pub fn with_capacity(nodes: usize, edges: usize) -> CallGraph {
        CallGraph {
            calls: DiGraph::with_capacity(nodes, edges),
        }
    }

    /// Create a new call-graph with the given number of nodes and edge capacity
    pub fn with_nodes(nodes: usize, edges: usize) -> CallGraph {
        let mut graph = CallGraph::with_capacity(nodes, edges);
        for _ in 0..nodes {
            graph.add_node()
        }
        graph
    }

    /// Add an edge to this call-graph
    ///
    /// If `filter_nonterminating` is true, `add_edge` may return `false` and leave the call-graph unchanged if it can
    /// be determined that adding the given edge would make some definition in the call-graph nonterminating.
    pub fn add_edge(
        &mut self,
        caller: usize,
        callee: usize,
        call: CallMat,
        filter_nonterminating: bool,
    ) -> bool {
        if filter_nonterminating && call.is_unknown() {
            return false;
        }
        let caller_ix = NodeIndex::new(caller);
        let callee_ix = NodeIndex::new(callee);
        self.calls.add_edge(caller_ix, callee_ix, call);
        true
    }

    /// Iterate over the edges between the caller and callee in this call-graph
    pub fn edges(
        &self,
        caller: usize,
        callee: usize,
    ) -> impl Iterator<Item = &CallMat> + Clone + '_ {
        let caller_ix = NodeIndex::new(caller);
        let callee_ix = NodeIndex::new(callee);
        self.calls
            .edges_connecting(caller_ix, callee_ix)
            .map(|edge| edge.weight())
    }

    /// Add a node to this call-graph
    pub fn add_node(&mut self) {
        self.calls.add_node(());
    }

    /// Condense this call-graph into a definition-graph, returning the first potentially non-terminating component, if any.
    ///
    /// If `early_fail` is true, don't bother termination-checking components after the first potentially non-terminating component.
    pub fn condense(self, early_fail: bool) -> (DefinitionGraph, Option<usize>) {
        let sccs = petgraph::algo::kosaraju_scc(&self.calls);
        let mut components = Vec::with_capacity(self.calls.node_count());
        let mut definition_graph = DiGraph::with_capacity(sccs.len(), 0);
        let mut subcalls = Vec::with_capacity(sccs.len());

        // Node construction
        for _ in 0..self.calls.node_count() {
            components.push((0, 0));
        }
        for (i, scc) in sccs.iter().enumerate() {
            for (j, ix) in scc.iter().enumerate() {
                components[ix.index()] = (i, j);
            }
            definition_graph.add_node(None);
            subcalls.push(DenseCallGraph::with_nodes(scc.len()));
        }

        // Edge construction
        let (_nodes, edges) = self.calls.into_nodes_edges();
        for edge in edges {
            let src = edge.source().index();
            let trg = edge.target().index();
            let (src_scc, src_ix) = components[src];
            let (trg_scc, trg_ix) = components[trg];
            if src_scc == trg_scc {
                subcalls[src_scc].add_edge(src_ix, trg_ix, edge.weight);
            } else {
                definition_graph.update_edge(NodeIndex::new(src_scc), NodeIndex::new(trg_scc), ());
            }
        }

        let mut failure = None;

        for (ix, (node_weight, mut subcalls)) in definition_graph
            .node_weights_mut()
            .zip(subcalls.drain(..))
            .enumerate()
        {
            subcalls.complete();
            let recursion_behaviours = subcalls.direct_recursion_behaviour();
            let mut decreasing_args = DecreasingArgs::new(recursion_behaviours.len());
            let mut terminating = true;
            for (def, recursion_behaviour) in recursion_behaviours.enumerate() {
                if recursion_behaviour.has_termination_order() {
                    for decreasing_arg in recursion_behaviour.decreasing_args() {
                        decreasing_args.push(def, decreasing_arg)
                    }
                } else {
                    terminating = false;
                    break;
                }
            }
            if terminating {
                *node_weight = Some(decreasing_args)
            } else {
                if failure.is_none() {
                    failure = Some(ix);
                }
                if early_fail {
                    break;
                }
            }
        }

        (
            DefinitionGraph {
                definitions: definition_graph,
                component_map: components,
            },
            failure,
        )
    }
}

/// A recursive dependency between mutually recursive definition
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
struct RecursiveDep {
    /// The caller
    pub caller: usize,
    /// The callee
    pub callee: usize,
    /// The caller argument being used
    pub caller_arg: usize,
    /// The callee argument
    pub callee_arg: usize,
}

#[cfg(test)]
mod test {
    use ndarray::array;

    use super::*;

    /// Example 5.1 from the [foetus](http://www2.tcs.ifi.lmu.de/~abel/foetus.pdf) paper
    #[test]
    fn foetus_example_5_1() {
        use Relation::*;
        let recursion_behaviour = RecursionBehaviour(array![
            [Equal, Less, Unknown],
            [Equal, Equal, Less],
            [Equal, Less, Equal]
        ]);
        assert!(recursion_behaviour.equiv(&recursion_behaviour));
        assert!(!recursion_behaviour.is_termination_order(&[]));

        let type_1_order = [0, 1, 2];
        let type_2_order = [1, 2, 0];
        assert!(recursion_behaviour.is_termination_order(&type_1_order));
        assert!(recursion_behaviour.is_termination_order(&type_2_order));

        assert!(!recursion_behaviour.is_termination_order(&type_1_order[..2]));
        assert!(recursion_behaviour.is_termination_order(&type_2_order[..2]));

        assert!(!recursion_behaviour.is_termination_order(&type_1_order[..1]));
        assert!(!recursion_behaviour.is_termination_order(&type_2_order[..1]));

        let not_an_order = [2, 1, 0];
        assert!(!recursion_behaviour.is_termination_order(&not_an_order));
        assert!(!recursion_behaviour.is_termination_order(&not_an_order[..2]));
        assert!(!recursion_behaviour.is_termination_order(&not_an_order[..1]));

        let termination_order = recursion_behaviour.termination_order().unwrap();
        assert_eq!(&termination_order.0[..], [1, 2]);
    }

    /// Example 3.1 from the [foetus](http://www2.tcs.ifi.lmu.de/~abel/foetus.pdf) paper (Addition and multiplication)
    #[test]
    fn foetus_example_3_1() {
        use Relation::*;

        let add_ix = 0;
        let mul_ix = 1;
        let mut call_graph = CallGraph::with_nodes(2, 3);

        call_graph.add_edge(
            add_ix,
            add_ix,
            CallMat::new_sorted(2, 2, [(0, 0, Less), (1, 1, Equal)]),
            false,
        );
        call_graph.add_edge(
            mul_ix,
            add_ix,
            CallMat::new_sorted(2, 2, [(0, 1, Equal)]),
            false,
        );
        call_graph.add_edge(
            mul_ix,
            mul_ix,
            CallMat::new_sorted(2, 2, [(0, 0, Less), (0, 1, Equal)]),
            false,
        );

        let (definition_graph, failure) = call_graph.condense(false);
        assert_eq!(failure, None);
        assert_eq!(definition_graph.no_components(), 2);
        assert_eq!(definition_graph.no_defs(), 2);
        assert_eq!(definition_graph.component(add_ix).unwrap(), (0, 0));
        assert_eq!(definition_graph.component(mul_ix).unwrap(), (1, 0));
        assert_eq!(
            definition_graph.decreasing_args(add_ix).unwrap().unwrap(),
            &[0]
        );
        assert_eq!(
            definition_graph.decreasing_args(mul_ix).unwrap().unwrap(),
            &[0]
        );
    }

    /// Example 3.12 from the [foetus](http://www2.tcs.ifi.lmu.de/~abel/foetus.pdf) paper (Non-terminating mutual recursion)
    #[test]
    fn foetus_example_3_12() {
        use Relation::*;
        let f = RecursionBehaviour(array![
            [Less, Unknown],
            [Unknown, Unknown],
            [Less, Equal],
            [Unknown, Less],
        ]);
        let g = RecursionBehaviour(array![[Unknown, Unknown], [Less, Equal], [Less, Unknown]]);
        let h = RecursionBehaviour(array![[Less, Less], [Less, Equal], [Equal, Less]]);

        assert!(f.equiv(&f));
        assert!(!f.equiv(&g));
        assert!(!f.equiv(&h));
        assert!(!g.equiv(&f));
        assert!(g.equiv(&g));
        assert!(!g.equiv(&h));
        assert!(!h.equiv(&f));
        assert!(!h.equiv(&g));
        assert!(h.equiv(&h));

        assert_eq!(f.termination_order(), None);
        assert_eq!(g.termination_order(), None);
        assert!(h.has_termination_order());

        let f_ix = 0;
        let g_ix = 1;
        let h_ix = 2;
        let mut call_graph = CallGraph::with_nodes(3, 6);

        call_graph.add_edge(
            f_ix,
            g_ix,
            CallMat::new_sorted(2, 2, [(0, 0, Less), (1, 1, Equal)]),
            false,
        );
        call_graph.add_edge(f_ix, g_ix, CallMat::new_sorted(2, 2, [(1, 1, Less)]), false);
        call_graph.add_edge(
            g_ix,
            f_ix,
            CallMat::new_sorted(2, 2, [(0, 0, Equal), (1, 1, Equal)]),
            false,
        );
        call_graph.add_edge(g_ix, g_ix, CallMat::new_sorted(2, 2, [(0, 0, Less)]), false);
        call_graph.add_edge(
            h_ix,
            h_ix,
            CallMat::new_sorted(2, 2, [(0, 0, Equal), (1, 1, Less)]),
            false,
        );
        call_graph.add_edge(
            h_ix,
            h_ix,
            CallMat::new_sorted(2, 2, [(0, 0, Less), (1, 1, Equal)]),
            false,
        );

        let (definition_graph, failure) = call_graph.condense(false);
        assert_eq!(failure, Some(1));
        assert_eq!(definition_graph.no_components(), 2);
        assert_eq!(definition_graph.no_defs(), 3);
        let (f_c, f_i) = definition_graph.component(f_ix).unwrap();
        let (g_c, g_i) = definition_graph.component(g_ix).unwrap();
        assert_eq!((0, 0), definition_graph.component(h_ix).unwrap());
        assert_eq!(f_c, 1);
        assert_eq!(g_c, 1);
        assert_ne!(f_i, g_i);
        assert!(f_i == 0 || g_i == 0);
        assert!(f_i == 1 || g_i == 1);

        assert_eq!(definition_graph.decreasing_args(0), Some(None));
        assert_eq!(definition_graph.decreasing_args(1), Some(None));
        assert_eq!(
            definition_graph.decreasing_args(2).unwrap().unwrap(),
            &[0, 1]
        );
    }

    #[test]
    fn single_second_arg_decreasing() {
        use Relation::*;
        let rec = RecursionBehaviour(array![[Equal, Less]]);
        assert!(!rec.is_decreasing_arg(0));
        assert!(rec.is_decreasing_arg(1));
        assert_eq!(rec.decreasing_args().collect::<Vec<_>>(), [1]);
    }

    /// The function
    /// ```text
    /// #let bad_mul: nats -> nats -> nats =  
    ///        λm: nats => λn: nats => #case nats (λ_:nats => nats) zero (λn' => add m (bad_mul m n)) n;
    /// ```
    #[test]
    fn bad_mul() {
        use Relation::*;
        let mut call_graph = CallGraph::with_nodes(1, 1);
        call_graph.add_edge(
            0,
            0,
            CallMat::new_sorted(2, 2, [(0, 0, Equal), (1, 1, Equal)]),
            false,
        );
        let (definition_graph, failure) = call_graph.condense(false);
        assert_eq!(failure, Some(0));
        assert_eq!(definition_graph.decreasing_args(0), Some(None));
    }
}
