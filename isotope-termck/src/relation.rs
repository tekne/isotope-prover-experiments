use num_traits::{MulAdd, One, Zero};
use smallvec::smallvec;
use smallvec::SmallVec;
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::ops::{Add, AddAssign, Mul, MulAssign};

/// A relation between terms and variables
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Relation {
    /// Unknown
    Unknown,
    /// Equal
    Equal,
    /// Less than
    Less,
}

impl PartialOrd for Relation {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (this, other) if this == other => Some(Ordering::Equal),
            (Relation::Unknown, _) => Some(Ordering::Less),
            (_, Relation::Unknown) => Some(Ordering::Greater),
            _ => None,
        }
    }
}

impl Relation {
    /// A slice containing all possible relations
    pub const RELATIONS: &'static [Relation] =
        &[Relation::Unknown, Relation::Equal, Relation::Less];

    /// Compute the meet of two relations
    pub fn meet(self, other: Relation) -> Relation {
        if self == other {
            self
        } else {
            Relation::Unknown
        }
    }
}

impl Default for Relation {
    #[inline]
    fn default() -> Self {
        Relation::Unknown
    }
}

impl Add for Relation {
    type Output = Relation;

    #[inline]
    fn add(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Relation::Unknown, rhs) => rhs,
            (Relation::Equal, Relation::Less) => Relation::Less,
            (Relation::Equal, _) => Relation::Equal,
            (Relation::Less, _) => Relation::Less,
        }
    }
}

impl Add<&'_ Relation> for Relation {
    type Output = Relation;

    #[inline]
    fn add(self, rhs: &Self) -> Self::Output {
        self.add(*rhs)
    }
}

impl Add<Relation> for &'_ Relation {
    type Output = Relation;

    #[inline]
    fn add(self, rhs: Relation) -> Self::Output {
        (*self).add(rhs)
    }
}

impl<'a, 'b> Add<&'a Relation> for &'b Relation {
    type Output = Relation;

    #[inline]
    fn add(self, rhs: &'a Relation) -> Self::Output {
        (*self).add(*rhs)
    }
}

impl Mul for Relation {
    type Output = Relation;

    #[inline]
    fn mul(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Relation::Unknown, _) => Relation::Unknown,
            (Relation::Equal, rhs) => rhs,
            (Relation::Less, Relation::Unknown) => Relation::Unknown,
            (Relation::Less, _) => Relation::Less,
        }
    }
}

impl Mul<&'_ Relation> for Relation {
    type Output = Relation;

    #[inline]
    fn mul(self, rhs: &Self) -> Self::Output {
        self.mul(*rhs)
    }
}

impl Mul<Relation> for &'_ Relation {
    type Output = Relation;

    #[inline]
    fn mul(self, rhs: Relation) -> Self::Output {
        (*self).mul(rhs)
    }
}

impl<'a, 'b> Mul<&'a Relation> for &'b Relation {
    type Output = Relation;

    #[inline]
    fn mul(self, rhs: &'a Relation) -> Self::Output {
        (*self).mul(*rhs)
    }
}

impl AddAssign for Relation {
    #[inline]
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs
    }
}

impl MulAssign for Relation {
    #[inline]
    fn mul_assign(&mut self, rhs: Self) {
        *self = *self * rhs
    }
}

impl Zero for Relation {
    #[inline]
    fn zero() -> Self {
        Relation::Unknown
    }

    #[inline]
    fn is_zero(&self) -> bool {
        *self == Relation::Unknown
    }
}

impl One for Relation {
    #[inline]
    fn one() -> Self {
        Relation::Equal
    }
}

impl MulAdd for Relation {
    type Output = Relation;

    #[inline]
    fn mul_add(self, a: Self, b: Self) -> Self::Output {
        self * a + b
    }
}

/// A single argument-relation pair for a variable
#[derive(Debug, Copy, Clone, Eq, Default)]
pub struct ArgRelation<I> {
    /// The argument
    pub arg: I,
    /// The relation to the argument
    pub rel: Relation,
}

impl<I> IntoIterator for ArgRelation<I> {
    type Item = (I, Relation);

    type IntoIter = std::option::IntoIter<(I, Relation)>;

    fn into_iter(self) -> Self::IntoIter {
        if self.rel != Relation::Unknown {
            Some((self.arg, self.rel))
        } else {
            None
        }
        .into_iter()
    }
}

impl<I: PartialEq> PartialOrd for ArgRelation<I> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.arg == other.arg {
            self.rel.partial_cmp(&other.rel)
        } else if self.rel == Relation::Unknown {
            if other.rel == Relation::Unknown {
                Some(Ordering::Less)
            } else {
                Some(Ordering::Equal)
            }
        } else if other.rel == Relation::Unknown {
            Some(Ordering::Greater)
        } else {
            None
        }
    }
}

impl<I> ArgRelation<I> {
    /// Check whether this set of relations is all unknown
    #[inline]
    pub fn is_unknown(&self) -> bool {
        self.rel == Relation::Unknown
    }

    /// Set this relation-set to it's meet with another
    #[inline]
    pub fn meet_with(&mut self, other: &Self)
    where
        I: PartialEq,
    {
        if self.arg == other.arg {
            self.rel = self.rel.meet(other.rel)
        } else {
            self.rel = Relation::Unknown
        }
    }
}

impl<I: PartialEq> PartialEq for ArgRelation<I> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.rel == other.rel && (self.rel == Relation::Unknown || self.arg == other.arg)
    }
}

impl<I: Hash> Hash for ArgRelation<I> {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.rel.hash(state);
        if self.rel != Relation::Unknown {
            self.arg.hash(state);
        }
    }
}

impl<I: Copy> Mul<&'_ ArgRelation<I>> for Relation {
    type Output = ArgRelation<I>;

    #[inline]
    fn mul(self, rhs: &ArgRelation<I>) -> Self::Output {
        self * (*rhs)
    }
}

impl<I: Copy> Mul<ArgRelation<I>> for Relation {
    type Output = ArgRelation<I>;

    #[inline]
    fn mul(self, mut rhs: ArgRelation<I>) -> Self::Output {
        rhs.rel *= self;
        rhs
    }
}

/// A sorted vector of argument-relation pairs for a variable
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Relations<I>(SmallVec<[ArgRelation<I>; 2]>);

impl<I> Default for Relations<I> {
    #[inline]
    fn default() -> Self {
        Relations(SmallVec::new())
    }
}

impl<I> Relations<I> {
    /// Create a new set of relations, summing over duplicate entries
    #[inline]
    pub fn new(mut relations: SmallVec<[ArgRelation<I>; 2]>) -> Relations<I>
    where
        I: Ord,
    {
        relations.sort_unstable_by(|left, right| left.arg.cmp(&right.arg));
        relations.dedup_by(|remove, keep| {
            if remove.arg == keep.arg {
                keep.rel += remove.rel;
                true
            } else {
                false
            }
        });
        relations.retain(|arg_rel| arg_rel.rel != Relation::Unknown);
        Relations(relations)
    }

    /// Create a new, singleton set of relations
    #[inline]
    pub fn singleton(arg: I, rel: Relation) -> Relations<I> {
        if rel == Relation::Unknown {
            return Relations(SmallVec::new());
        }
        Relations(smallvec![ArgRelation { arg, rel }])
    }

    /// Check whether this set of relations is all unknown
    #[inline]
    pub fn is_unknown(&self) -> bool {
        self.0.is_empty()
    }

    /// Set this relation-set to it's meet with another
    #[inline]
    pub fn meet_with(&mut self, other: &Self)
    where
        I: Ord,
    {
        //TODO: optimize?
        self.0.retain(|this| {
            let other_rel = other
                .0
                .binary_search_by_key(&&this.arg, |other| &other.arg)
                .map(|ix| other.0[ix].rel)
                .unwrap_or_default();
            this.rel = this.rel.meet(other_rel);
            this.rel != Relation::Unknown
        })
    }
}

impl<I: Clone> Mul<&'_ Relations<I>> for Relation {
    type Output = Relations<I>;

    #[inline]
    fn mul(self, rhs: &Relations<I>) -> Self::Output {
        let mut result = Relations::default();
        if self != Relation::Unknown {
            for ArgRelation { arg, rel } in &rhs.0 {
                let rel = self * rel;
                if rel != Relation::Unknown {
                    result.0.push(ArgRelation {
                        arg: arg.clone(),
                        rel,
                    })
                }
            }
        }
        result
    }
}

impl<I: Clone> Mul<Relations<I>> for Relation {
    type Output = Relations<I>;

    #[inline]
    fn mul(self, mut rhs: Relations<I>) -> Self::Output {
        if self == Relation::Unknown {
            rhs.0.clear();
        } else {
            for arg_rel in &mut rhs.0 {
                arg_rel.rel *= self;
            }
            rhs.0.retain(|arg_rel| arg_rel.rel != Relation::Unknown);
        }
        rhs
    }
}

impl<I> From<ArgRelation<I>> for Relations<I> {
    #[inline]
    fn from(arg_rel: ArgRelation<I>) -> Self {
        Relations::singleton(arg_rel.arg, arg_rel.rel)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn relation_arithmetic() {
        for &a in Relation::RELATIONS {
            assert_eq!(Relation::zero() + a, a);
            assert_eq!(Relation::one() * a, a);
            for &b in Relation::RELATIONS {
                assert_eq!(a + b, b + a);
                assert_eq!(a * b, b * a);
                assert_eq!(a + b, &a + b);
                assert_eq!(a + b, a + &b);
                assert_eq!(a + b, &a + &b);
                assert_eq!(a * b, &a * b);
                assert_eq!(a * b, a * &b);
                assert_eq!(a * b, &a * &b);
                let mut tmp = a;
                tmp += b;
                assert_eq!(tmp, a + b);
                let mut tmp = a;
                tmp *= b;
                assert_eq!(tmp, a * b);

                for &c in Relation::RELATIONS {
                    assert_eq!(a.mul_add(b, c), a * b + c);
                    assert_eq!(a + (b + c), (a + b) + c);
                    assert_eq!(a * (b * c), (a * b) * c);
                    assert_eq!(a * (b + c), a * b + a * c);
                }
            }
        }
    }
}
