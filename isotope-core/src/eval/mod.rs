/*!
Evaluation contexts and utilities
*/

use crate::*;

/// An evaluation context
pub trait EvalCtx: SubstCtx {
    /// Push an optionally constant substitution onto this evaluation context
    fn push_subst(
        &mut self,
        arg: TermId,
        param_ty: Option<&TermId>,
        is_const: bool,
    ) -> Result<(), Error>;
    /// Push a nil substitution onto this evaluation context
    fn push_nil(&mut self) -> Result<(), Error>;
    /// Pop an optionally constant substitution from this evaluation context
    fn pop_subst(&mut self, is_const: bool) -> Result<(), Error>;
}
