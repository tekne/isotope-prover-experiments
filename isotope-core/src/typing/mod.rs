/*!
Typing contexts for `isotope` terms
*/
use crate::*;

/// A mutable typing and/or unification context for `isotope` terms
pub trait TyCtxMut {
    /// Try to typecheck a term
    #[inline(always)]
    fn try_tyck_mut(&mut self, _term: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        Err(())
    }

    /// Register an equality constraint
    fn constrain_mut(&mut self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool>;

    /// Register a typechecked term
    #[inline(always)]
    fn register_tyck_mut(&mut self, _term: &TermRef) -> Result<(), Error> {
        Ok(())
    }
}

/// A typing and/or unification context for `isotope` terms
pub trait TyCtx: TyCtxMut {
    /// Try to typecheck a term
    #[inline(always)]
    fn try_tyck(&self, _term: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        Err(())
    }

    /// Register an equality constraint
    fn constrain(&self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool>;

    /// Register a typechecked term
    #[inline(always)]
    fn register_tyck(&self, _term: &TermRef) -> Result<(), Error> {
        Ok(())
    }
}

impl<T> TyCtxMut for &'_ mut T
where
    T: TyCtxMut,
{
    #[inline(always)]
    fn try_tyck_mut(&mut self, term: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        (**self).try_tyck_mut(term, fail_early)
    }

    #[inline(always)]
    fn constrain_mut(&mut self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool> {
        (**self).constrain_mut(left, right, fail_early)
    }

    #[inline(always)]
    fn register_tyck_mut(&mut self, term: &TermRef) -> Result<(), Error> {
        (**self).register_tyck_mut(term)
    }
}

impl<T> TyCtx for &'_ mut T
where
    T: TyCtx,
{
    #[inline(always)]
    fn try_tyck(&self, term: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        (**self).try_tyck(term, fail_early)
    }

    #[inline(always)]
    fn constrain(&self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool> {
        (**self).constrain(left, right, fail_early)
    }

    #[inline(always)]
    fn register_tyck(&self, term: &TermRef) -> Result<(), Error> {
        (**self).register_tyck(term)
    }
}

impl<T> TyCtxMut for &'_ T
where
    T: TyCtx,
{
    #[inline(always)]
    fn try_tyck_mut(&mut self, term: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        (**self).try_tyck(term, fail_early)
    }

    #[inline(always)]
    fn constrain_mut(&mut self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool> {
        (**self).constrain(left, right, fail_early)
    }

    #[inline]
    fn register_tyck_mut(&mut self, term: &TermRef) -> Result<(), Error> {
        (**self).register_tyck(term)
    }
}

impl<T> TyCtx for &'_ T
where
    T: TyCtx,
{
    #[inline(always)]
    fn try_tyck(&self, term: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        (**self).try_tyck(term, fail_early)
    }

    #[inline(always)]
    fn constrain(&self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool> {
        (**self).constrain(left, right, fail_early)
    }

    #[inline]
    fn register_tyck(&self, term: &TermRef) -> Result<(), Error> {
        (**self).register_tyck(term)
    }
}

/// Get an equality context as a typing context
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd, Default, ref_cast::RefCast)]
#[repr(transparent)]
pub struct AsTyCtx<E>(pub E);

impl<E> TyCtxMut for AsTyCtx<E>
where
    E: EqCtxMut,
{
    #[inline]
    fn try_tyck_mut(&mut self, term: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        if term.nil_tyck() {
            Ok(Some(true))
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn constrain_mut(&mut self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool> {
        left.term_eq_mut(right, fail_early, &mut self.0)
    }
}

impl<E> TyCtx for AsTyCtx<E>
where
    E: EqCtx,
{
    #[inline]
    fn try_tyck(&self, term: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        if term.nil_tyck() {
            Ok(Some(true))
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn constrain(&self, left: &TermRef, right: &TermRef, fail_early: bool) -> Option<bool> {
        left.term_eq(right, fail_early, &self.0)
    }
}

/// A typing equation necessary for a term
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct TyckEqn<'a>((TermRef<'a>, TermRef<'a>));

impl<'a> TyckEqn<'a> {
    /// Create a new typing equation between the left and right terms given
    pub fn new(left: TermRef<'a>, right: TermRef<'a>) -> Option<TyckEqn<'a>> {
        match left.cmp(&right) {
            Ordering::Less => Some(TyckEqn((left, right))),
            Ordering::Equal => None,
            Ordering::Greater => Some(TyckEqn((right, left))),
        }
    }

    /// Convert this typing equation into an owned equation
    ///
    pub fn into_owned(self) -> TyckEqn<'static> {
        TyckEqn((self.0 .0.into_owned(), self.0 .1.into_owned()))
    }

    /// Get the left-hand side of this equation
    pub fn left(&self) -> &TermRef<'a> {
        &self.0 .0
    }

    /// Get the right-hand side of this equation
    pub fn right(&self) -> &TermRef<'a> {
        &self.0 .1
    }

    //TODO: full call-by-need checking?

    //TODO: head-normal call-by-need checking?

    /// Check this equation using full, call-by-value normalization
    pub fn check_cbv(
        &self,
        steps: &mut u64,
        fail_early: bool,
        eq: &mut impl EqCtxMut,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<bool>, Error> {
        if let Some(result) = self.0 .0.term_eq_mut(&self.0 .1, fail_early, eq) {
            return Ok(Some(result));
        }
        Ok(self.0 .0.norm_cons_in(steps, ctx)?.term_eq_mut(
            &self.0 .1.norm_cons_in(steps, ctx)?,
            fail_early,
            eq,
        ))
    }
}

impl<'a> Deref for TyckEqn<'a> {
    type Target = (TermRef<'a>, TermRef<'a>);

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// A the typing equations necessary to typecheck a term, modulo some equality context
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct TyckEqns<E = Structural> {
    /// The equality context to filter these equations modulo
    pub ctx: E,
    /// The terms which have already been seen
    pub seen: hashbrown::HashSet<TermId>,
    /// The typing equations necessary to typecheck a term
    pub eqns: hashbrown::HashSet<TyckEqn<'static>>,
}

impl<E> TyckEqns<E> {
    /// Check this set of equations in the given evaluation context using full, call-by-value normalization
    pub fn check_cbv_with(
        //TODO: generalize
        eqns: &hashbrown::HashSet<TyckEqn>,
        steps: u64,
        fail_early: bool,
        eq: &mut impl EqCtxMut,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<bool>, Error> {
        let mut failed = false;
        for eqn in eqns.iter() {
            let mut steps = steps;
            match eqn.check_cbv(&mut steps, fail_early, eq, ctx)? {
                Some(true) => {}
                Some(false) => return Ok(Some(false)),
                None if fail_early => return Ok(None),
                None => failed = true,
            }
        }
        Ok(if failed { None } else { Some(true) })
    }

    /// Check this set of equations *in parallel* in the given evaluation context using full, call-by-value normalization
    #[cfg(feature = "parallel")]
    pub fn check_cbv_parallel_with<C>(
        //TODO: generalize
        eqns: &hashbrown::HashSet<TyckEqn>,
        steps: u64,
        fail_early: bool,
        eq: &mut impl EqCtxMut,
        ctx: impl Send + Sync + Fn() -> C,
    ) -> Result<Option<bool>, Error>
    where
        C: EvalCtx,
    {
        use congruence::DisjointSetForest;
        use indexmap::IndexMap;
        use rayon::prelude::*;
        let mut indices = IndexMap::new();
        let mut dsf = DisjointSetForest::new();
        for eqn in eqns.iter() {
            let (left, _) = indices.insert_full(eqn.left().borrow_id(), eqn.left().borrow_id());
            let (right, _) = indices.insert_full(eqn.right().borrow_id(), eqn.right().borrow_id());
            dsf.union(left, right)
        }
        indices
            .par_iter_mut()
            .try_for_each(|(term, normed)| -> Result<(), Error> {
                let mut steps = steps;
                if let Some(norm) = term.norm_in(&mut steps, &mut ctx())? {
                    *normed = norm;
                }
                Ok(())
            })?;
        let mut failed = false;
        for (ix, (_term, normed)) in indices.iter().enumerate() {
            match indices[dsf.find_mut(ix)].term_eq_mut(normed, fail_early, eq) {
                Some(true) => {}
                Some(false) => return Ok(Some(false)),
                None if fail_early => return Ok(None),
                None => failed = true,
            }
        }
        Ok(if failed { None } else { Some(true) })
    }
}

impl<E> TyckEqns<E>
where
    E: EqCtxMut,
{
    /// Check this set of equations in the given evaluation context using full, call-by-value normalization
    pub fn check_cbv(
        &mut self,
        steps: u64,
        fail_early: bool,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<bool>, Error> {
        Self::check_cbv_with(&self.eqns, steps, fail_early, &mut self.ctx, ctx)
    }

    /// Check this set of equations *in parallel* in the given evaluation context using full, call-by-value normalization
    #[cfg(feature = "parallel")]
    pub fn check_cbv_parallel<C>(
        &mut self,
        steps: u64,
        fail_early: bool,
        ctx: impl Send + Sync + Fn() -> C,
    ) -> Result<Option<bool>, Error>
    where
        C: EvalCtx,
    {
        Self::check_cbv_parallel_with(&self.eqns, steps, fail_early, &mut self.ctx, ctx)
    }
}

impl<E> TyCtxMut for TyckEqns<E>
where
    E: EqCtxMut,
{
    #[inline]
    fn try_tyck_mut(&mut self, term: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        if term.nil_tyck() || self.seen.contains(term) {
            return Ok(Some(true));
        }
        Err(())
    }

    #[inline]
    fn constrain_mut(
        &mut self,
        left: &TermRef,
        right: &TermRef,
        _fail_early: bool,
    ) -> Option<bool> {
        if !left
            .term_eq_mut(right, true, &mut self.ctx)
            .unwrap_or(false)
        {
            if let Some(eqn) = TyckEqn::new(left.borrow_id(), right.borrow_id()) {
                self.eqns.insert(eqn.into_owned());
            }
        }
        Some(true)
    }

    #[inline]
    fn register_tyck_mut(&mut self, term: &TermRef) -> Result<(), Error> {
        self.seen.insert(term.clone_into_owned());
        Ok(())
    }
}
