/*!
Utilities for comparing terms
*/
use crate::*;

mod cong;
mod dsf;

pub use cong::CongruenceCtx;
pub use congruence::UnionFind;
pub use dsf::ForestCtx;
use ref_cast::RefCast;

/// A trait defining an equivalence relation between terms
pub trait EqCtxMut {
    /// The underlying equality context
    type EqCtxMut: EqCtxMut;

    /// Whether this context ignores irrelevant terms
    #[inline(always)]
    fn ignore_irr(&self) -> bool {
        false
    }

    /// Try to compare two terms in a mutable context
    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        fail_early: bool,
    ) -> Result<Option<bool>, ()>;

    /// Mutably get the underlying equality context, if possible
    fn eq_ctx_mut(&mut self) -> &mut Self::EqCtxMut;

    /// Mutably get the underlying equality context as a typing context, if possible
    #[inline(always)]
    fn ty_ctx_mut(&mut self) -> &mut AsTyCtx<Self::EqCtxMut> {
        RefCast::ref_cast_mut(self.eq_ctx_mut())
    }
}

/// A trait defining an equivalence relation between terms
pub trait EqCtx: EqCtxMut {
    /// The underlying equality context
    type EqCtx: EqCtx;

    /// Try to compare two terms
    fn try_eq(&self, left: &Term, right: &Term, fail_early: bool) -> Result<Option<bool>, ()>;

    /// Get the underlying equality context
    fn eq_ctx(&self) -> &Self::EqCtx;

    /// Get the underlying equality context as a typing context
    #[inline(always)]
    fn ty_ctx(&self) -> &AsTyCtx<Self::EqCtx> {
        RefCast::ref_cast(self.eq_ctx())
    }
}

impl<E> EqCtxMut for &'_ E
where
    E: EqCtx,
{
    type EqCtxMut = Self;

    #[inline]
    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        fail_early: bool,
    ) -> Result<Option<bool>, ()> {
        (**self).try_eq(left, right, fail_early)
    }

    #[inline]
    fn eq_ctx_mut(&mut self) -> &mut Self {
        self
    }
}

impl<E> EqCtx for &'_ E
where
    E: EqCtx,
{
    type EqCtx = E::EqCtx;

    #[inline]
    fn try_eq(&self, left: &Term, right: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        (**self).try_eq(left, right, fail_early)
    }

    #[inline]
    fn eq_ctx(&self) -> &Self::EqCtx {
        (**self).eq_ctx()
    }
}

impl<E> EqCtxMut for &'_ mut E
where
    E: EqCtxMut,
{
    type EqCtxMut = E::EqCtxMut;

    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        fail_early: bool,
    ) -> Result<Option<bool>, ()> {
        (**self).try_eq_mut(left, right, fail_early)
    }

    fn eq_ctx_mut(&mut self) -> &mut Self::EqCtxMut {
        (**self).eq_ctx_mut()
    }
}

impl<E> EqCtx for &'_ mut E
where
    E: EqCtx,
{
    type EqCtx = E::EqCtx;

    fn try_eq(&self, left: &Term, right: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        (**self).try_eq(left, right, fail_early)
    }

    fn eq_ctx(&self) -> &Self::EqCtx {
        (**self).eq_ctx()
    }
}

/// Something which can be compared modulo a term congruence relation
pub trait Equiv<T: ?Sized> {
    /// Compare values modulo a term congruence relation
    fn term_eq(&self, other: &T, fail_early: bool, ctx: &impl EqCtx) -> Option<bool> {
        self.term_eq_mut(other, fail_early, &mut ctx.eq_ctx())
    }

    /// Compare values modulo a term congruence relation
    fn term_eq_mut(&self, other: &T, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool>;
}

impl<T, E> Equiv<E> for &'_ T
where
    T: Equiv<E>,
{
    fn term_eq_mut(&self, other: &E, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        (*self).term_eq_mut(other, fail_early, ctx)
    }
}

impl<T, E> Equiv<Option<E>> for Option<T>
where
    T: Equiv<E>,
{
    fn term_eq_mut(
        &self,
        other: &Option<E>,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        match (self, other) {
            (Some(this), Some(other)) => this.term_eq_mut(other, fail_early, ctx),
            (None, None) => Some(true),
            _ => Some(false),
        }
    }
}

impl<T, U> Equiv<[U]> for [T]
where
    T: Equiv<U>,
{
    fn term_eq_mut(&self, other: &[U], fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        if self.len() != other.len() {
            return Some(false);
        }
        let mut determinate = true;
        for (this, other) in self.iter().zip(other.iter()) {
            match this.term_eq_mut(other, fail_early, ctx) {
                Some(false) => return Some(false),
                None => determinate = false,
                _ => {}
            }
        }
        if determinate {
            Some(true)
        } else {
            None
        }
    }
}

/// Structural equality
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd, Default)]
pub struct Structural(pub bool);

impl EqCtxMut for Structural {
    type EqCtxMut = Structural;

    #[inline(always)]
    fn ignore_irr(&self) -> bool {
        self.0
    }

    #[inline(always)]
    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        fail_early: bool,
    ) -> Result<Option<bool>, ()> {
        self.try_eq(left, right, fail_early)
    }

    #[inline(always)]
    fn eq_ctx_mut(&mut self) -> &mut Self::EqCtxMut {
        self
    }
}

impl EqCtx for Structural {
    type EqCtx = Structural;

    #[inline]
    fn try_eq(&self, left: &Term, right: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            Ok(Some(true))
        } else if left.code() != right.code() {
            Ok(Some(false))
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn eq_ctx(&self) -> &Self::EqCtx {
        self
    }
}

/// Equality modulo some relation
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct EqMod<R>(R, bool);

impl<R> EqCtxMut for EqMod<R>
where
    R: for<'a> FnMut(&'a Term, &'a Term) -> Option<bool>,
{
    type EqCtxMut = Self;

    #[inline(always)]
    fn ignore_irr(&self) -> bool {
        self.1
    }

    #[inline]
    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        _fail_early: bool,
    ) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            Ok(Some(true))
        } else if let Some(result) = self.0(left, right) {
            Ok(Some(result))
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn eq_ctx_mut(&mut self) -> &mut Self::EqCtxMut {
        self
    }
}

impl<R> EqCtx for EqMod<R>
where
    R: for<'a> Fn(&'a Term, &'a Term) -> Option<bool>,
{
    type EqCtx = Self;

    #[inline]
    fn try_eq(&self, left: &Term, right: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            Ok(Some(true))
        } else if let Some(result) = self.0(left, right) {
            Ok(Some(result))
        } else {
            Err(())
        }
    }

    #[inline]
    fn eq_ctx(&self) -> &Self::EqCtx {
        self
    }
}

/// Generate implementations of `Equiv<Term>` from an `Equiv<V>` implementation, where `V` is a variant of `Term`
#[macro_export]
macro_rules! equiv_term {
    ($value:ident) => {
        impl Equiv<Term> for $value {
            fn term_eq_mut(
                &self,
                other: &Term,
                fail_early: bool,
                ctx: &mut impl EqCtxMut,
            ) -> Option<bool> {
                match other {
                    Term::$value(other) => self.term_eq_mut(other, fail_early, ctx),
                    other => {
                        if self.is_unique_head() && other.is_unique_head() {
                            Some(false)
                        } else {
                            None
                        }
                    }
                }
            }
        }

        impl PartialEq<Term> for $value {
            #[inline]
            fn eq(&self, other: &Term) -> bool {
                match other {
                    Term::$value(other) => self.eq(other),
                    _ => false,
                }
            }
        }

        impl PartialEq<$value> for Term {
            #[inline(always)]
            fn eq(&self, other: &$value) -> bool {
                other.eq(self)
            }
        }
    };
}

/// Generate implementations of `Equiv<Term>` and `PartialEq<Term>` from `Equiv<V>` and `PartialEq` implementations, where `V` is a variant of `Term`
#[macro_export]
macro_rules! eq_term {
    ($value:ident) => {
        equiv_term!($value);
    };
}

/// Generate implementations of `Equiv<TermId>` from an `Equiv<Term>` implementation
#[macro_export]
macro_rules! equiv_termid {
    ($value:ty) => {
        impl Equiv<TermRef<'_>> for $value {
            #[inline(always)]
            fn term_eq_mut(
                &self,
                other: &TermRef<'_>,
                fail_early: bool,
                ctx: &mut impl EqCtxMut,
            ) -> Option<bool> {
                self.term_eq_mut(&**other, fail_early, ctx)
            }
        }
    };
}

/// Generate implementations of `Equiv<TermId>` and `PartialEq<TermId>` from `Equiv<Term>` and `PartialEq<Term>` implementations
#[macro_export]
macro_rules! eq_termid {
    ($value:ty) => {
        equiv_termid!($value);

        impl PartialEq<TermRef<'_>> for $value {
            #[inline(always)]
            fn eq(&self, other: &TermRef<'_>) -> bool {
                self.eq(&**other)
            }
        }

        impl PartialEq<$value> for TermRef<'_> {
            #[inline(always)]
            fn eq(&self, other: &$value) -> bool {
                other.eq(self)
            }
        }
    };
}
