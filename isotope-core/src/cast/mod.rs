/*!
Casting isotope terms between surface types
*/
use crate::*;

/// Values which may be cast between types
pub trait Cast<C>: Cons<C> {
    /// Cast this value into another (judgementally equal) type
    ///
    /// Note that using this to cast into a supertype is invalid!
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<C>, Error>;

    /// Get this value, cast this value into another (judgementally equal) type
    ///
    /// Note that using this to cast into a supertype is invalid!
    #[inline]
    fn cast_consed(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<C, Error> {
        Ok(self.cast(ty, ctx)?.unwrap_or_else(|| self.consed(ctx)))
    }
}

impl<T, C> Cast<C> for &'_ T
where
    T: Cast<C>,
{
    #[inline]
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<C>, Error> {
        (*self).cast(ty, ctx)
    }
}

/// Generate implementations of `Cast<Term>` and `Cast<TermId>` from a `Cast<V>` implementation, where `V: Value`
#[macro_export]
macro_rules! value_cast {
    ($value:ty) => {
        impl Cast<Term> for $value {
            fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<Term>, Error> {
                let v: Option<$value> = self.cast(ty, ctx)?;
                if let Some(v) = v {
                    Ok(Some(v.into_term()))
                } else {
                    Ok(None)
                }
            }
        }

        impl Cast<TermId> for $value {
            fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<TermId>, Error> {
                let v: Option<$value> = self.cast(ty, ctx)?;
                if let Some(v) = v {
                    Ok(Some(TermId::shallow_cons(v.into_term(), ctx)))
                } else {
                    Ok(None)
                }
            }
        }
    };
}
