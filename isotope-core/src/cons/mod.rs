/*!
Hash-consing for terms
*/
use crate::*;

mod hash;
mod index;
pub use hash::HashCtx;
pub use index::IndexCtx;
use smallvec::Array;

/// A context for hash-consing terms
pub trait ConsCtx {
    /// This term's dereferenced form
    type ConsCtx: ConsCtx;

    /// Get whether a term is consed
    fn is_consed(&self, term: &TermRef<'_>) -> bool;

    /// Try to cons a `Term`
    fn try_cons(&self, term: &Term) -> Option<&TermId>;

    /// Try to cons a `Term`
    fn try_cons_mut(&mut self, term: &Term) -> Option<&TermId> {
        self.try_cons(term)
    }

    /// Shallow-cons a term ID
    fn shallow_cons(&mut self, id: TermRef<'_>) -> Option<TermId>;

    /// Whether this context is pointer-exact, i.e., guarantees consed terms will be pointer-equal
    fn pointer_exact(&self) -> bool;

    /// Get a mutable reference to this context
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx;

    /// Try to uncons a `Term`, given a code
    fn uncons(&self, _code: Code) -> Option<TermId> {
        None
    }
}

impl ConsCtx for () {
    type ConsCtx = ();

    #[inline]
    fn is_consed(&self, _term: &TermRef<'_>) -> bool {
        true
    }

    #[inline]
    fn try_cons(&self, _term: &Term) -> Option<&TermId> {
        None
    }

    #[inline]
    fn try_cons_mut(&mut self, _term: &Term) -> Option<&TermId> {
        None
    }

    #[inline]
    fn shallow_cons(&mut self, _id: TermRef<'_>) -> Option<TermId> {
        None
    }

    #[inline]
    fn pointer_exact(&self) -> bool {
        false
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self
    }

    fn uncons(&self, _code: Code) -> Option<TermId> {
        None
    }
}

impl<C> ConsCtx for &'_ mut C
where
    C: ConsCtx,
{
    type ConsCtx = C::ConsCtx;

    #[inline]
    fn is_consed(&self, term: &TermRef<'_>) -> bool {
        (**self).is_consed(term)
    }

    #[inline]
    fn try_cons(&self, term: &Term) -> Option<&TermId> {
        (**self).try_cons(term)
    }

    #[inline]
    fn try_cons_mut(&mut self, term: &Term) -> Option<&TermId> {
        (**self).try_cons_mut(term)
    }

    #[inline]
    fn shallow_cons(&mut self, id: TermRef<'_>) -> Option<TermId> {
        (**self).shallow_cons(id)
    }

    #[inline]
    fn pointer_exact(&self) -> bool {
        (**self).pointer_exact()
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        (**self).cons_ctx()
    }

    #[inline]
    fn uncons(&self, code: Code) -> Option<TermId> {
        (**self).uncons(code)
    }
}

/// A value which can be consed
pub trait Cons<C> {
    /// Cons this value
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<C>;
    /// Get this value, consed
    fn consed(&self, ctx: &mut impl ConsCtx) -> C;
    /// Get this value, *shallow-consed* (i.e., `cons` returned `None`)
    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> C;
}

impl<T, C> Cons<C> for &'_ T
where
    T: Cons<C>,
{
    #[inline]
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<C> {
        (*self).cons(ctx)
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> C {
        (*self).consed(ctx)
    }

    #[inline]
    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> C {
        (*self).shallow_consed(ctx)
    }
}

impl<T, C> Cons<Option<C>> for Option<T>
where
    T: Cons<C>,
{
    #[inline]
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<Option<C>> {
        if let Some(this) = self {
            this.cons(ctx).map(Some)
        } else {
            None
        }
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> Option<C> {
        if let Some(this) = self {
            Some(this.consed(ctx))
        } else {
            None
        }
    }

    #[inline]
    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> Option<C> {
        if let Some(this) = self {
            Some(this.shallow_consed(ctx))
        } else {
            None
        }
    }
}

fn cons_arr_helper<T, C, const N: usize>(
    this: &[T; N],
    mut trip: bool,
    shallow: bool,
    zero: bool,
    ctx: &mut impl ConsCtx,
) -> Option<[C; N]>
where
    T: Cons<C>,
{
    let mut result = ArrayVec::<C, N>::new();
    for (i, elem) in this.iter().enumerate() {
        if let Some(cons) = if shallow {
            Some(elem.shallow_consed(ctx))
        } else {
            elem.cons(ctx)
        } {
            while i > result.len() {
                debug_assert!(!trip && !shallow);
                result.push(this[result.len()].shallow_consed(ctx))
            }
            trip = true;
            result.push(cons)
        } else if !result.is_empty() || trip {
            result.push(elem.shallow_consed(ctx))
        }
    }
    if let Ok(result) = result.into_inner() {
        debug_assert!(result.len() == 0 || trip);
        if zero && result.len() == 0 {
            None
        } else {
            Some(result)
        }
    } else {
        debug_assert!(!trip);
        None
    }
}

impl<T, C, const N: usize> Cons<[C; N]> for [T; N]
where
    T: Cons<C>,
{
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<[C; N]> {
        cons_arr_helper(self, false, false, true, ctx)
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> [C; N] {
        cons_arr_helper(self, true, false, false, ctx).unwrap()
    }

    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> [C; N] {
        cons_arr_helper(self, true, true, false, ctx).unwrap()
    }
}

fn cons_small_helper<C, T>(
    this: &[T],
    mut trip: bool,
    shallow: bool,
    zero: bool,
    ctx: &mut impl ConsCtx,
) -> Option<SmallVec<C>>
where
    C: Array,
    T: Cons<C::Item>,
{
    let mut result = SmallVec::<C>::new();
    for (i, elem) in this.iter().enumerate() {
        if let Some(cons) = if shallow {
            Some(elem.shallow_consed(ctx))
        } else {
            elem.cons(ctx)
        } {
            if result.is_empty() {
                result.reserve(this.len());
            }
            while i > result.len() {
                debug_assert!(!trip && !shallow);
                result.push(this[result.len()].shallow_consed(ctx))
            }
            trip = true;
            result.push(cons)
        } else if !result.is_empty() || trip {
            if result.is_empty() {
                result.reserve(this.len());
            }
            result.push(elem.shallow_consed(ctx))
        }
    }
    if result.len() == this.len() {
        if result.is_empty() && zero {
            None
        } else {
            Some(result)
        }
    } else {
        None
    }
}

impl<A, C> Cons<SmallVec<C>> for SmallVec<A>
where
    A: Array,
    C: Array,
    A::Item: Cons<C::Item>,
{
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<SmallVec<C>> {
        cons_small_helper(self, false, false, true, ctx)
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> SmallVec<C> {
        cons_small_helper(self, true, false, false, ctx).unwrap()
    }

    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> SmallVec<C> {
        cons_small_helper(self, true, true, false, ctx).unwrap()
    }
}

impl<L, R, CL, CR> Cons<(CL, CR)> for (L, R)
where
    L: Cons<CL>,
    R: Cons<CR>,
{
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<(CL, CR)> {
        let l = self.0.cons(ctx);
        let r = self.1.cons(ctx);
        if l.is_none() && r.is_none() {
            None
        } else {
            Some((
                l.unwrap_or_else(|| self.0.shallow_consed(ctx)),
                r.unwrap_or_else(|| self.1.shallow_consed(ctx)),
            ))
        }
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> (CL, CR) {
        (self.0.consed(ctx), self.1.consed(ctx))
    }

    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> (CL, CR) {
        (self.0.shallow_consed(ctx), self.1.shallow_consed(ctx))
    }
}

/// Generate an implementation of `Cons<V>` given a function `cons` of type `Fn(&V, ctx: &mut impl ConsCtx) -> Option<V>`, where `V: Clone`
#[macro_export]
macro_rules! self_cons {
    ($V:ty; $this:ident, $ctx:ident => $cons:expr) => {
        impl Cons<$V> for $V {
            fn cons(&self, ctx: &mut impl ConsCtx) -> Option<$V> {
                let $this = self;
                let $ctx = ctx;
                $cons
            }

            fn consed(&self, ctx: &mut impl ConsCtx) -> $V {
                if let Some(cons) = self.cons(ctx) {
                    cons
                } else {
                    self.clone()
                }
            }

            fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> $V {
                self.clone()
            }
        }
    };
}

/// Log a collision between an unconsed term ID and a value, if they have the same code
#[cold]
#[inline(never)]
pub fn log_collision<T: Value>(id: &TermRef, collision: &T) {
    use std::sync::atomic::{AtomicU64, Ordering::Relaxed};
    static TYPE_COLLISIONS: AtomicU64 = AtomicU64::new(0);

    debug_assert_eq!(id.code(), collision.code());
    if id.code() == collision.code() {
        let collisions = TYPE_COLLISIONS.fetch_add(1, Relaxed);
        if collisions == 0 || collisions.is_power_of_two() {
            warn!(
                "Detected {} code collisions for type {}",
                collisions,
                std::any::type_name::<T>()
            );
        }
    }
}

/// Generate implementations of `Cons<Term>` and `Cons<TermId>` from a `Cons<V>` implementation, where `V: Value`
#[macro_export]
macro_rules! value_cons {
    ($value:ty) => {
        impl Cons<Term> for $value {
            fn cons(&self, ctx: &mut impl ConsCtx) -> Option<Term> {
                let v: $value = self.cons(ctx)?;
                Some(v.into_term())
            }

            fn consed(&self, ctx: &mut impl ConsCtx) -> Term {
                let v: $value = self.consed(ctx);
                v.into_term()
            }

            fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> Term {
                let v: $value = self.shallow_consed(ctx);
                v.into_term()
            }
        }

        impl Cons<TermId> for $value {
            fn cons(&self, ctx: &mut impl ConsCtx) -> Option<TermId> {
                if let Some(id) = ctx.uncons(self.code()) {
                    if *self == *id {
                        return Some(id.clone());
                    }
                    log_collision(&id, self);
                }
                let v: $value = self.cons(ctx)?;
                Some(TermId::shallow_cons(v.into_term(), ctx))
            }

            fn consed(&self, ctx: &mut impl ConsCtx) -> TermId {
                if let Some(id) = ctx.uncons(self.code()) {
                    if *self == *id {
                        return id.clone();
                    }
                    log_collision(&id, self);
                }
                let v: $value = self.consed(ctx);
                TermId::shallow_cons(v.into_term(), ctx)
            }

            fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> TermId {
                if let Some(id) = ctx.uncons(self.code()) {
                    if *self == *id {
                        return id.clone();
                    }
                    log_collision(&id, self);
                }
                let v: $value = self.shallow_consed(ctx);
                TermId::shallow_cons(v.into_term(), ctx)
            }
        }
    };
}

/// Generate and use a collection of values, consed within a given context, or return a failure value if all said values are already consed
#[macro_export]
macro_rules! do_cons {
    (in $ctx:ident; $(let $c:ident$(: $C:ty)? = $e:expr;)* else $f:expr; $r:expr) => {{
        $(let $c = $e.cons($ctx).ok_or(&$e);)*
        if false $(|| $c.is_ok())* {
            $(let $c$(: $C)? = match $c {
                Ok(c) => c,
                Err(c) => (*c).shallow_consed($ctx)
            };)*
            $r
        } else {
            $f
        }
    }};
    (in $ctx:ident; $(let $c:ident$(: $C:ty)? = $e:expr;)* $r:expr) => {{
        do_cons!(in $ctx; $(let $c$(: $C)? = $e;)* else None; $r)
    }}
}
