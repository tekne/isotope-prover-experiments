/*!
Utilities for `isotope`
*/

use std::fmt::{self, Debug};
use std::hash::Hasher;

pub mod argv;
pub mod bloom;
pub mod sparse_stack;

/// A hash-code
#[derive(Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd)]
pub struct Code(pub u64);

impl Debug for Code {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}

/// Propagate false results
#[macro_export]
macro_rules! prop_false {
    ($e:expr) => {
        match $e {
            Some(false) => return Some(false),
            other => other,
        }
    };
}

/// Lazily compute the conjunction of optional boolean expressions
#[macro_export]
macro_rules! opt_and {
    ($fail_early:expr; $($e:expr),+) => {
        loop {
            let fail_early = $fail_early;
            let mut has_none = false;
            $(match $e {
                Some(false) => break Some(false),
                Some(true) => {},
                None => {
                    has_none = true;
                    if fail_early {
                        break None
                    }
                }
            })*
            break if has_none {
                None
            } else {
                Some(true)
            }
        }
    }
}

/// A binary result, which is either left, right, or this
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Binary<T> {
    /// Return this value
    This(T),
    /// Return the value on the left
    Left,
    /// Return the value on the right
    Right,
}

impl<T> Binary<T> {
    /// Map this binary result to another type
    #[inline]
    pub fn map<U>(self, f: impl FnOnce(T) -> U) -> Binary<U> {
        match self {
            Binary::This(this) => Binary::This(f(this)),
            Binary::Left => Binary::Left,
            Binary::Right => Binary::Right,
        }
    }

    /// Flip a binary result
    #[inline]
    pub fn flip(self) -> Binary<T> {
        match self {
            Binary::This(this) => Binary::This(this),
            Binary::Left => Binary::Right,
            Binary::Right => Binary::Left,
        }
    }

    /// Convert this binary result to it's underlying type, cloning from a left and right source as necessary
    #[inline]
    pub fn unwrap_or_clone(self, left: &T, right: &T) -> T
    where
        T: Clone,
    {
        match self {
            Binary::This(this) => this,
            Binary::Left => left.clone(),
            Binary::Right => right.clone(),
        }
    }
}

/// A pass-through hasher
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Default)]
pub struct Passthrough(pub u64);

impl Hasher for Passthrough {
    #[inline]
    fn finish(&self) -> u64 {
        self.0
    }

    #[inline]
    fn write(&mut self, bytes: &[u8]) {
        let start = bytes.len() - bytes.len().saturating_sub(8);
        for &byte in &bytes[start..] {
            self.write_u8(byte)
        }
    }

    #[inline]
    fn write_u8(&mut self, i: u8) {
        self.0 = self.0.wrapping_shr(8) | i as u64;
    }

    #[inline]
    fn write_u16(&mut self, i: u16) {
        self.0 = self.0.wrapping_shr(16) | i as u64;
    }

    #[inline]
    fn write_u32(&mut self, i: u32) {
        self.0 = self.0.wrapping_shr(32) | i as u64;
    }

    #[inline]
    fn write_u64(&mut self, i: u64) {
        self.0 = i
    }

    #[inline]
    fn write_u128(&mut self, i: u128) {
        self.0 = i as u64;
    }

    #[inline]
    fn write_usize(&mut self, i: usize) {
        self.0 = self.0.wrapping_shr(usize::BITS) | i as u64;
    }
}
