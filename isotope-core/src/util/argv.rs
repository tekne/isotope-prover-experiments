/*!
A trait for argument vectors
*/
use smallvec::Array;

use crate::*;

/// A trait implemented by vectors of optional arguments
pub trait ArgVec<T = TermId> {
    /// Get the length of this argument vector
    fn arg_len(&self) -> usize;
    
    /// Index this argument vector from the *back*
    fn arg(&self, ix: usize) -> Option<&T>;

    /// Push an argument onto this vector, returning an error on failure
    fn push_arg(&mut self, arg: T) -> Result<(), Error>;

    /// Pop an argument from this vector, returning an error on failure
    fn pop_arg(&mut self) -> Result<Option<T>, Error>;
}

impl<T> ArgVec<T> for Vec<T> {
    #[inline]
    fn arg_len(&self) -> usize {
        self.len()
    }

    #[inline]
    fn arg(&self, ix: usize) -> Option<&T> {
        if ix >= self.len() {
            None
        } else {
            self.get((self.len() - ix) - 1)
        }
    }

    #[inline]
    fn push_arg(&mut self, arg: T) -> Result<(), Error> {
        self.push(arg);
        Ok(())
    }

    #[inline]
    fn pop_arg(&mut self) -> Result<Option<T>, Error> {
        Ok(self.pop())
    }
}

impl<A> ArgVec<A::Item> for SmallVec<A>
where
    A: Array,
{
    #[inline]
    fn arg_len(&self) -> usize {
        self.len()
    }

    #[inline]
    fn arg(&self, ix: usize) -> Option<&A::Item> {
        if ix >= self.len() {
            None
        } else {
            self.get((self.len() - ix) - 1)
        }
    }

    #[inline]
    fn push_arg(&mut self, arg: A::Item) -> Result<(), Error> {
        self.push(arg);
        Ok(())
    }

    #[inline]
    fn pop_arg(&mut self) -> Result<Option<A::Item>, Error> {
        Ok(self.pop())
    }
}
