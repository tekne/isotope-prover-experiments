/*!
A Bloom filter for variable indices which keeps track of the maximum index inserted into it
*/
use crate::*;

/// The index type used for variables
pub type VarIx = u32;

/// The signed version of the index type used for variables
pub type SVarIx = i32;

/// A Bloom filter for variable indices which keeps track of the maximum index inserted into it
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Filter {
    /// The upper bound of this filter
    max: VarIx,
    /// The indices in this filter
    indices: u32,
}

impl Debug for Filter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Filter({}, 0b{:b})", self.max, self.indices)
    }
}

impl Filter {
    /// An empty filter
    pub const EMPTY: Filter = Filter {
        max: 0,
        indices: 0b0,
    };

    /// A full filter
    pub const FULL: Filter = Filter {
        max: VarIx::MAX,
        indices: u32::MAX,
    };

    /// The number of bits in this filter
    pub const BITS: VarIx = 32;

    /// Create a new, singleton filter
    #[inline]
    pub const fn singleton(ix: VarIx) -> Filter {
        Filter {
            max: ix,
            indices: 0b1 << (ix % Self::BITS) as u32,
        }
    }

    /// Take the union of this filter with another
    #[inline]
    pub const fn union(self, other: Filter) -> Filter {
        Filter {
            max: if self.max > other.max {
                self.max
            } else {
                other.max
            },
            indices: self.indices | other.indices,
        }
    }

    /// Insert an element into a filter
    #[inline]
    pub const fn insert(self, ix: VarIx) -> Filter {
        self.union(Self::singleton(ix))
    }

    /// Check whether a filter is empty
    #[inline]
    pub const fn is_empty(self) -> bool {
        self.max == 0 && self.indices == 0
    }

    /// Get whether a filter contains an index within a given equivalence class
    #[inline]
    pub const fn contains_equiv(self, equiv: VarIx) -> bool {
        self.indices & (0b1 << equiv % Self::BITS) != 0
    }

    /// Get the maximum index potentially in this filter
    #[inline]
    #[cfg(not(tarpaulin_include))]
    pub const fn max(self) -> VarIx {
        self.max
    }

    /// Get the lower bound for indices potentially in this filter
    #[inline]
    pub const fn min(self) -> VarIx {
        if self.max == 0 {
            //TODO: VarIx::MAX instead?
            0
        } else {
            self.indices.trailing_zeros() as VarIx
        }
    }

    /// Get whether a filter contains an index within a given equivalence class between `max` and `min`
    #[inline]
    pub const fn contains_within(self, equiv: VarIx, min: VarIx, max: VarIx) -> Option<bool> {
        if min > max || min > self.max || max < self.min() || !self.contains_equiv(equiv) {
            // Definitely false, as either there is no intersection with the desired range (min, max) or there is no member of the equivalence class at all
            Some(false)
        } else if self.max < Self::BITS
            || (equiv % Self::BITS == self.max % Self::BITS && self.max <= max)
        {
            // Definitely true, as either the filter is exact or the equivalence class is that of the maximal element (which is in the desired range (min, max))
            Some(true)
        } else {
            // Maybe true, maybe false
            None
        }
    }

    /// Get whether a filter contains an index
    #[inline]
    pub const fn contains(self, ix: VarIx) -> Option<bool> {
        self.contains_within(ix, ix, ix)
    }

    /// Get this filter shifted down given a checking function
    #[inline]
    pub fn shift_down(
        mut self,
        mut shift: VarIx,
        check: impl Fn(VarIx, VarIx, VarIx) -> bool,
    ) -> Filter {
        //TODO: optimize this... a lot!
        while shift != 0 {
            let mut shifted = Filter {
                max: self.max.saturating_sub(1),
                indices: self.indices.wrapping_shr(1),
            };
            let contains_hi_zero = self
                .contains_within(0, Self::BITS, VarIx::MAX)
                .unwrap_or_else(|| check(0, Self::BITS, VarIx::MAX));
            if contains_hi_zero {
                shifted.indices |= 0b1 << (Self::BITS - 1)
            };
            self = shifted;
            shift -= 1;
        }
        self
    }

    /// Get this filter shifted up
    #[inline]
    pub fn shift_up(self, shift: VarIx) -> Filter {
        Filter {
            max: if self.indices == 0 { 0 } else { self.max.saturating_add(shift) },
            indices: self.indices.rotate_left(1),
        }
    }

    /// Check whether this filter potentially intersects another
    #[inline]
    pub const fn intersects(self, other: Filter) -> Option<bool> {
        if self.indices & other.indices == 0 {
            Some(false)
        } else if self.max < Self::BITS && other.max < Self::BITS {
            Some(true)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn empty_filter_properties() {
        assert!(Filter::EMPTY.is_empty());
        assert_ne!(format!("{:?}", Filter::EMPTY), "");
        for i in 0..128 {
            assert_eq!(Filter::EMPTY.shift_up(i), Filter::EMPTY);
            assert!(!Filter::EMPTY.contains_equiv(i));
            assert_eq!(Filter::EMPTY.contains(i), Some(false));
            let fi = Filter::singleton(i);
            assert_eq!(Filter::EMPTY.insert(i), fi);
            assert_eq!(Filter::EMPTY.union(fi), fi);
            assert_eq!(fi.union(Filter::EMPTY), fi);
        }
    }

    #[test]
    fn singleton_filter_properties() {
        for i in 0..128 {
            let fi = Filter::singleton(i);
            assert!(!fi.is_empty());
            assert_eq!(fi.max(), i);
            assert_eq!(fi.min(), i % Filter::BITS);
            assert_eq!(fi.union(fi), fi);
            assert_ne!(format!("{:?}", fi), "");
            for j in 0..128 {
                if i < 32.min(Filter::BITS) {
                    assert_eq!(fi.contains(j), Some(j == i));
                } else {
                    assert_eq!(
                        fi.contains(j).unwrap_or(true),
                        j <= i && j % Filter::BITS == i % Filter::BITS
                    );
                }
                let fj = Filter::singleton(j);
                assert_eq!(fi == fj, i == j);
                let fij = fi.union(fj);
                assert_eq!(fij, fi.insert(j));
                assert_eq!(fij.max(), i.max(j));
                assert_eq!(fij.min(), (i % Filter::BITS).min(j % Filter::BITS));
                assert_eq!(fij == fi, i >= j && i % Filter::BITS == j % Filter::BITS);
                assert_eq!(fij == fj, i <= j && i % Filter::BITS == j % Filter::BITS);
                if i < 32.min(Filter::BITS) && j < 32.min(Filter::BITS) {
                    for k in 0..128 {
                        assert_eq!(fij.contains(k), Some(i == k || j == k))
                    }
                } else {
                    assert!(fij.contains(i).unwrap_or(true));
                    assert!(fij.contains(j).unwrap_or(true));
                }
            }
        }
    }
}
