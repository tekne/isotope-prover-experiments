/*!
An indexable ``sparse stack" data structure, which acts as a vector with gaps
*/
use crate::*;

/// An indexable "sparse stack" data structure, which is simply a vector of `Option<T>` with some operations optimized.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SparseStack<T> {
    /// The elements of this vector
    elems: Vec<T>,
    /// The runs of this vector
    runs: SmallVec<[Run; 1]>,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Run {
    /// The length at the end of this run
    end_len: usize,
    /// The beginning of this run in the element vector
    begin_ix: usize,
}

// === Sparse stack API ===

impl<T> SparseStack<T> {
    /// Create a new, empty sparse stack
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core::util::sparse_stack::SparseStack;
    /// let mut stack = SparseStack::<usize>::new();
    /// debug_assert!(stack.is_empty());
    /// debug_assert_eq!(stack.len(), 0);
    /// debug_assert_eq!(stack.no_elems(), 0);
    /// debug_assert_eq!(stack.no_runs(), 1);
    /// debug_assert_eq!(stack.capacity(), 0)
    /// ```
    #[inline(always)]
    pub fn new() -> SparseStack<T> {
        Self::default()
    }

    /// Get the length of this stack
    ///
    /// Note that this is equal to the total number of `Some` and `None` elements inserted, combined.
    #[inline]
    pub fn len(&self) -> usize {
        self.last_run().end_len
    }

    /// Get the capacity of this stack, in terms of number of elements
    ///
    /// Note that, unlike a normal `Vec`, we may have `capacity < len`, but never `capacity < no_elems`
    #[inline]
    pub fn capacity(&self) -> usize {
        self.elems.capacity()
    }

    /// Clear this stack, leaving it empty but maintaining it's capacity
    #[inline]
    pub fn clear(&mut self) {
        self.elems.clear();
        self.runs.clear();
        self.runs.push(Run {
            end_len: 0,
            begin_ix: 0,
        });
    }

    /// Get whether this stack is empty
    ///
    /// # Examples
    /// ```rust
    /// # use isotope_core::util::sparse_stack::SparseStack;
    /// let mut stack = SparseStack::<usize>::new();
    /// assert!(stack.is_empty());
    /// stack.push_none(1);
    /// assert!(!stack.is_empty());
    /// stack.clear();
    /// assert!(stack.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.last_run().end_len == 0
    }

    /// Get the number of non-`None` elements in this stack
    #[inline]
    pub fn no_elems(&self) -> usize {
        self.elems.len()
    }

    /// Get the number of contiguous runs of non-`None` elements in this stack
    #[inline]
    pub fn no_runs(&self) -> usize {
        self.runs.len()
    }

    /// Get the number of non-`None` elements before a given index in this stack
    #[inline]
    pub fn no_elems_before(&self, ix: usize) -> usize {
        self.get_ix(ix).unwrap_or_else(|err_ix| err_ix)
    }

    /// Get the number of `None` elements before a given index in this stack
    #[inline]
    pub fn no_gaps_before(&self, ix: usize) -> usize {
        ix - self.no_elems_before(ix)
    }

    /// Get the number of non-`None` elements after a given index in this stack
    #[inline]
    pub fn no_elems_after(&self, ix: usize) -> usize {
        self.no_elems() - self.no_elems_before(ix)
    }

    /// Get the number of `None` elements after a given index in this stack
    #[inline]
    pub fn no_gaps_after(&self, ix: usize) -> usize {
        self.no_gaps() - self.no_gaps_before(ix)
    }

    /// Get the number of gaps in this stack, i.e. positions holding `None`
    #[inline]
    pub fn no_gaps(&self) -> usize {
        self.len() - self.no_elems()
    }

    /// Push an element onto the end of this stack
    #[inline]
    pub fn push(&mut self, elem: Option<T>) {
        if let Some(elem) = elem {
            self.push_some(elem)
        } else {
            self.push_none(1)
        }
    }

    /// Push an element `Some(elem)` onto the end of this stack
    #[inline]
    pub fn push_some(&mut self, elem: T) {
        debug_assert!(!self.last_run_null());
        self.begin_pure_run();
        self.elems.push(elem);
        self.last_run_mut().end_len += 1;
        debug_assert!(!self.last_run_null());
    }

    /// Push `n` `None`s onto this stack
    #[inline]
    pub fn push_none(&mut self, n: usize) {
        debug_assert!(!self.last_run_null());
        self.last_run_mut().end_len += n;
        debug_assert!(!self.last_run_null());
    }

    /// Pop an element from this stack. Return `None` if there are no elements left *or* the next element is a `None`
    #[inline]
    pub fn pop_some(&mut self) -> Option<T> {
        debug_assert!(!self.last_run_null());
        if self.in_gap() {
            None
        } else {
            let result = self.elems.pop();
            if result.is_some() {
                self.last_run_mut().end_len -= 1;
            }
            self.remove_null();
            result
        }
    }

    /// Pop `n` `None`s from this stack.
    ///
    /// If `exact` is `false`, pop as many `None`s as possible up to `n`;
    /// if `exact` is true, return an error if there are less than `n` `None`s at the top of the stack.
    /// Return how many `None`s were popped.
    #[inline]
    pub fn pop_none(&mut self, n: usize, exact: bool) -> usize {
        debug_assert!(!self.last_run_null());
        let last_gap = self.last_gap();
        if exact && n > last_gap {
            return 0;
        }
        let popped = n.min(last_gap);
        self.last_run_mut().end_len -= popped;
        self.remove_null();
        popped
    }

    /// Get the length of the last gap, i.e. how many `None`'s are at the end of the stack
    #[inline]
    pub fn last_gap(&self) -> usize {
        self.last_run().end_len
            - self.begin_len(self.runs.len() - 1)
            - (self.elems.len() - self.last_run().begin_ix)
    }

    /// Get whether this vector is currently in a gap, i.e. whether the last element is `None`
    #[inline]
    pub fn in_gap(&self) -> bool {
        self.last_gap() != 0
    }

    /// Get the index for the value in the `n`th position of this stack, if any
    #[inline]
    pub fn get_ix(&self, n: usize) -> Result<usize, usize> {
        debug_assert!(!self.last_run_null());
        if n >= self.len() {
            return Err(self.no_elems());
        }
        let run_ix = match self.runs.binary_search_by_key(&n, |run| run.end_len - 1) {
            Ok(ix) => ix,
            Err(ix) => ix,
        };
        let run = self.runs.get(run_ix).ok_or(self.no_elems())?;
        let ix = run.begin_ix + (n - self.begin_len(run_ix));
        let run_end = self.end_ix(run_ix);
        if ix >= run_end {
            Err(run_end)
        } else {
            Ok(ix)
        }
    }

    /// Get the beginning length of a given run
    #[inline]
    pub fn begin_len(&self, run: usize) -> usize {
        if run == 0 {
            0
        } else {
            self.runs[run - 1].end_len
        }
    }

    /// Get the end length of a given run
    #[inline]
    pub fn end_len(&self, run: usize) -> usize {
        self.runs[run].end_len
    }

    /// Get the beginning index of a given run
    #[inline]
    pub fn begin_ix(&self, run: usize) -> usize {
        self.runs[run].begin_ix
    }

    /// Get the end index of a given run
    #[inline]
    pub fn end_ix(&self, run: usize) -> usize {
        debug_assert!(run < self.runs.len());
        if let Some(run) = self.runs.get(run + 1) {
            run.begin_ix
        } else {
            self.elems.len()
        }
    }

    /// Get the length of a run
    #[inline]
    pub fn run_len(&self, run: usize) -> usize {
        self.end_len(run) - self.begin_len(run)
    }

    /// Get the gap length of a run
    #[inline]
    pub fn run_gap(&self, run: usize) -> usize {
        self.run_len(run) - self.run_elems(run)
    }

    /// Get the number of elements in a run
    #[inline]
    pub fn run_elems(&self, run: usize) -> usize {
        self.end_ix(run) - self.begin_ix(run)
    }

    /// Get the value at the `n`th position of this stack, if any
    #[inline]
    pub fn get(&self, n: usize) -> Option<&T> {
        self.elems.get(self.get_ix(n).ok()?)
    }

    /// Mutably get the value at the `n`th position of this stack, if any
    #[inline]
    pub fn get_mut(&mut self, n: usize) -> Option<&mut T> {
        let ix = self.get_ix(n).ok()?;
        self.elems.get_mut(ix)
    }

    /// Get the elements in this stack
    #[inline]
    pub fn elems(&self) -> &[T] {
        debug_assert!(!self.last_run_null());
        &self.elems[..]
    }

    /// Mutably get the elements in this stack
    #[inline]
    pub fn elems_mut(&mut self) -> &mut [T] {
        debug_assert!(!self.last_run_null());
        &mut self.elems[..]
    }

    /// Get a run in this stack
    #[inline]
    pub fn run(&self, run: usize) -> Option<&[T]> {
        if run < self.runs.len() {
            Some(&self.elems[self.begin_ix(run)..self.end_ix(run)])
        } else {
            None
        }
    }

    /// Mutably get a run in this stack
    #[inline]
    pub fn run_mut(&mut self, run: usize) -> Option<&mut [T]> {
        if run < self.runs.len() {
            let begin = self.begin_ix(run);
            let end = self.end_ix(run);
            Some(&mut self.elems[begin..end])
        } else {
            None
        }
    }
}

impl<T> Index<usize> for SparseStack<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        self.get(index).unwrap()
    }
}

impl<T> IndexMut<usize> for SparseStack<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.get_mut(index).unwrap()
    }
}

// === Sparse stack implementation ===

impl<T> Default for SparseStack<T> {
    fn default() -> Self {
        Self {
            elems: Vec::default(),
            runs: smallvec![Run {
                end_len: 0,
                begin_ix: 0
            }],
        }
    }
}

impl<T> SparseStack<T> {
    /// Get the last run in this stack
    #[inline]
    fn last_run(&self) -> Run {
        *self
            .runs
            .last()
            .expect("A valid sparse stack should always contain at least one run")
    }
    /// Mutably get the last run in this stack
    #[inline]
    fn last_run_mut(&mut self) -> &mut Run {
        self.runs
            .last_mut()
            .expect("A valid sparse stack should always contain at least one run")
    }
    /// Check whether the last run in this stack is null
    #[inline]
    fn last_run_null(&self) -> bool {
        debug_assert_ne!(self.runs.len(), 0);
        if self.runs.len() <= 1 {
            false
        } else {
            self.runs[self.runs.len() - 2].end_len == self.runs[self.runs.len() - 1].end_len
        }
    }
    /// Fixup any null runs
    #[inline]
    fn remove_null(&mut self) {
        if self.last_run_null() {
            self.runs.pop();
        }
        debug_assert!(!self.last_run_null())
    }
    /// Begin a pure run, if necessary
    #[inline]
    fn begin_pure_run(&mut self) {
        debug_assert!(!self.last_run_null());
        if self.in_gap() {
            self.runs.push(Run {
                end_len: self.len(),
                begin_ix: self.elems.len(),
            });
            debug_assert!(self.last_run_null());
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn integer_stack() {
        let mut stack = SparseStack::default();
        let mut state = Vec::new();
        assert_eq!(stack.len(), 0);
        assert_eq!(stack.no_elems(), 0);
        assert_eq!(stack.no_gaps(), 0);
        stack.push_some(5);
        state.push(stack.clone());
        assert_eq!(stack.len(), 1);
        assert_eq!(stack.no_elems(), 1);
        assert_eq!(stack.no_gaps(), 0);
        assert_eq!(stack[0], 5);
        assert_eq!(stack.run(0), Some(stack.elems()));
        stack.push_some(6);
        state.push(stack.clone());
        assert_eq!(stack.len(), 2);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 0);
        assert_eq!(stack[0], 5);
        assert_eq!(stack[1], 6);
        assert_eq!(stack.get(2), None);
        assert_eq!(stack.get(3), None);
        assert_eq!(stack.get(4), None);
        assert_eq!(stack.get_ix(2), Err(2));
        assert_eq!(stack.get_ix(3), Err(2));
        assert_eq!(stack.get_ix(4), Err(2));
        assert_eq!(stack.run(0), Some(stack.elems()));
        stack.push_none(3);
        state.push(stack.clone());
        assert_eq!(stack.len(), 5);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 3);
        assert_eq!(stack[0], 5);
        assert_eq!(stack[1], 6);
        assert_eq!(stack.get(2), None);
        assert_eq!(stack.get(3), None);
        assert_eq!(stack.get(4), None);
        assert_eq!(stack.get_ix(2), Err(2));
        assert_eq!(stack.get_ix(3), Err(2));
        assert_eq!(stack.get_ix(4), Err(2));
        assert_eq!(stack.run(0), Some(stack.elems()));
        stack.push_some(7);
        state.push(stack.clone());
        assert_eq!(stack[0], 5);
        assert_eq!(stack[1], 6);
        assert_eq!(stack.get(2), None);
        assert_eq!(stack.get(3), None);
        assert_eq!(stack.get(4), None);
        assert_eq!(stack.get_ix(2), Err(2));
        assert_eq!(stack.get_ix(3), Err(2));
        assert_eq!(stack.get_ix(4), Err(2));
        assert_eq!(stack[5], 7);
        assert_eq!(stack.run(0), Some(&stack.elems()[0..2]));
        assert_eq!(stack.run(1), Some(&stack.elems()[2..]));
        assert_eq!(stack.pop_none(10, false), 0);
        assert_eq!(stack, state.pop().unwrap());
        assert_eq!(stack.pop_some(), Some(7));
        assert_eq!(stack, *state.last().unwrap());
        assert_eq!(stack.pop_some(), None);
        assert_eq!(stack, *state.last().unwrap());
        assert_eq!(stack.pop_none(5, true), 0);
        assert_eq!(stack, state.pop().unwrap());
        assert_eq!(stack.pop_none(1, true), 1);
        assert_eq!(stack.len(), 4);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 2);
        assert_eq!(stack.pop_some(), None);
        assert_eq!(stack.pop_none(1, false), 1);
        assert_eq!(stack.len(), 3);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 1);
        assert_eq!(stack.pop_some(), None);
        assert_eq!(stack.pop_none(2, true), 0);
        assert_eq!(stack.pop_some(), None);
        assert_eq!(stack.pop_none(2, false), 1);
        assert_eq!(stack.len(), 2);
        assert_eq!(stack.no_elems(), 2);
        assert_eq!(stack.no_gaps(), 0);
        assert_eq!(stack, state.pop().unwrap());
        assert_eq!(stack.pop_some(), Some(6));
        assert_eq!(stack, state.pop().unwrap());
        assert_eq!(stack.pop_some(), Some(5));
        assert!(stack.is_empty());
    }

    #[test]
    fn one_element_elem_ix() {
        let mut stack = SparseStack::default();
        stack.push_some(1);
        assert_eq!(stack.get_ix(0), Ok(0));
        assert_eq!(stack.get_ix(1), Err(1));
        stack.push_none(1);
        assert_eq!(stack.get_ix(0), Ok(0));
        assert_eq!(stack.get_ix(1), Err(1));
    }
}
