/*!
Vector-based substitution context
*/
use crate::*;

/// A stack-based substitution context. Performs only very rudimentary type-checking; in particular, variable coherence and path coherence are *not* checked!
#[derive(Debug, Clone)]
pub struct SubstStack<C> {
    /// The substitution stack in use
    pub stack: SparseStack<Option<TermId>>,
    /// The underlying `ConsCtx` to use
    pub cons: C,
}

//TODO: fixme

impl<C: Default> Default for SubstStack<C> {
    fn default() -> Self {
        Self {
            stack: Default::default(),
            cons: Default::default(),
        }
    }
}

impl<C> SubstStack<C>
where
    C: ConsCtx,
{
    /// Create a new substitution context given a cons context
    pub fn new(cons: C) -> SubstStack<C> {
        SubstStack {
            stack: SparseStack::default(),
            cons,
        }
    }

    /// Check whether this substitution context is null
    pub fn is_null(&self) -> bool {
        self.stack.is_empty()
    }
}

impl<C> SubstCtx for SubstStack<C>
where
    C: ConsCtx,
{
    type ConsCtx = C::ConsCtx;

    #[inline]
    fn is_null(&self) -> bool {
        self.stack.no_elems() == 0
    }

    fn try_subst(&mut self, term: &Term) -> Result<Option<Option<TermId>>, Error> {
        if (term.fv().max() as usize) < self.stack.run_gap(self.stack.no_runs() - 1) {
            Ok(Some(None))
        } else {
            Ok(None)
        }
    }

    fn subst_ix(
        &mut self,
        ix: VarIx,
        base_ty: Option<&TermId>,
        ty: Option<&TermId>,
    ) -> Result<Option<TermId>, Error> {
        let elem_ix = if ix as usize >= self.stack.len() {
            Err(self.stack.no_elems())
        } else {
            self.stack.get_ix(self.stack.len() - 1 - ix as usize)
        };
        match elem_ix {
            Ok(elem_ix) => {
                if let Some(elem) = self.stack.elems()[elem_ix].clone() {
                    let rev_ix = self.stack.len() - 1 - ix as usize;
                    let no_gaps_before = rev_ix - elem_ix;
                    let shift = self.stack.no_gaps() - no_gaps_before;
                    debug_assert_eq!(self.stack.no_gaps_after(rev_ix), shift);
                    let elem = elem.shifted(shift as SVarIx, 0, self.cons.cons_ctx())?;
                    if let Some(ty) = ty.subst_cons(self)? {
                        elem.cast_consed(ty, self.cons.cons_ctx()).map(Some)
                    } else {
                        Ok(Some(elem))
                    }
                } else {
                    Err(Error::CannotInfer)
                }
            }
            Err(elem_ix) => {
                let rev_elem_ix = self.stack.no_elems() - elem_ix;
                let shift = rev_elem_ix as VarIx;
                if shift as usize != rev_elem_ix {
                    return Err(Error::VarIxOverflow);
                } else if shift == 0 {
                    Ok(None)
                } else {
                    //TODO: optimize case where ty == base_ty?
                    Ok(Some(
                        Var::try_new(ix - shift, base_ty.subst_cons(self)?, ty.subst_cons(self)?)?
                            .into_id_in(self.cons.cons_ctx()),
                    ))
                }
            }
        }
    }

    #[inline]
    fn push_param(&mut self, _ty: Option<&TermId>) -> Result<(), Error> {
        self.push_params(1)
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Error> {
        self.pop_params(1)
    }

    #[inline]
    fn push_params(&mut self, n: VarIx) -> Result<(), Error> {
        self.stack.push_none(n as usize);
        Ok(())
    }

    #[inline]
    fn pop_params(&mut self, n: VarIx) -> Result<(), Error> {
        let popped = self.stack.pop_none(n as usize, true);
        if popped != n as usize {
            debug_assert_eq!(popped, 0);
            Err(Error::ParamUnderflow)
        } else {
            Ok(())
        }
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self.cons.cons_ctx()
    }
}

impl<C> EvalCtx for SubstStack<C>
where
    C: ConsCtx,
{
    fn push_subst(
        &mut self,
        arg: TermId,
        _param_ty: Option<&TermId>,
        is_const: bool,
    ) -> Result<(), Error> {
        if !is_const {
            self.stack.push_some(Some(arg));
        }
        Ok(())
    }

    fn push_nil(&mut self) -> Result<(), Error> {
        self.stack.push_some(None);
        Ok(())
    }

    fn pop_subst(&mut self, is_const: bool) -> Result<(), Error> {
        if is_const || self.stack.pop_some().is_some() {
            Ok(())
        } else {
            //TODO: Special error for substitution underflow?
            Err(Error::ParamUnderflow)
        }
    }
}
