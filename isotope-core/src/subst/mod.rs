/*!
Substitution contexts and utilities
*/
use crate::*;

pub mod shift;
pub mod slice;
pub mod stack;

pub use shift::Shift;
use smallvec::Array;
pub use stack::SubstStack;

/// A substitution context
pub trait SubstCtx {
    /// This context's underlying cons context
    type ConsCtx: ConsCtx;

    /// Get whether this context is null
    fn is_null(&self) -> bool;
    /// Try to substitute a term
    fn try_subst(&mut self, term: &Term) -> Result<Option<Option<TermId>>, Error>;
    /// Substitute a variable index with a given annotation
    fn subst_ix(
        &mut self,
        ix: VarIx,
        base_ty: Option<&TermId>,
        ty: Option<&TermId>,
    ) -> Result<Option<TermId>, Error>;
    /// Push a de-Bruijn indexed parameter onto this context
    fn push_param(&mut self, ty: Option<&TermId>) -> Result<(), Error>;
    /// Pop a de-Bruijn indexed parameter from this context
    fn pop_param(&mut self) -> Result<(), Error>;
    /// Push `n` untyped, non-constant de-Bruijn indexed parameters onto this context
    fn push_params(&mut self, n: VarIx) -> Result<(), Error>;
    /// Pop `n` untyped, non-constant de-Bruijn indexed parameters from this context
    fn pop_params(&mut self, n: VarIx) -> Result<(), Error>;
    /// Get this context's underlying cons context
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx;
    /// Cache a substitution. May yield an invalid context if called with a bad substitution
    #[inline]
    fn cache_subst(&mut self, _term: &Term, _subst: Option<&TermId>) -> Result<(), Error> {
        Ok(())
    }
}

/// A value which can be substituted to yield a value of type `S`
pub trait Subst<S>: Cons<S> {
    /// Substitute this value's subterms
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<S>, Error>;

    /// Get this value substituted and consed in a given context
    #[inline]
    fn subst_cons(&self, ctx: &mut impl SubstCtx) -> Result<S, Error> {
        Ok(self
            .subst(ctx)?
            .unwrap_or_else(|| self.consed(ctx.cons_ctx())))
    }

    /// Get this value, shifted by `shift` with base `base`
    #[inline]
    fn shifted(&self, shift: SVarIx, base: VarIx, ctx: &mut impl ConsCtx) -> Result<S, Error> {
        Ok(self
            .subst(&mut Shift {
                shift,
                base,
                cons: ctx.cons_ctx(),
            })?
            .unwrap_or_else(|| self.consed(ctx.cons_ctx())))
    }
}

impl<T, S> Subst<S> for &'_ T
where
    T: Subst<S>,
{
    #[inline]
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<S>, Error> {
        (*self).subst(ctx)
    }
}

impl<T, S> Subst<Option<S>> for Option<T>
where
    T: Subst<S>,
{
    #[inline]
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Option<S>>, Error> {
        if let Some(this) = self {
            match this.subst(ctx) {
                Ok(Some(this)) => Ok(Some(Some(this))),
                Ok(None) => Ok(None),
                Err(err) => Err(err),
            }
        } else {
            Ok(None)
        }
    }
}

fn subst_small_helper<C, T>(
    this: &[T],
    mut trip: bool,
    ctx: &mut impl SubstCtx,
) -> Result<Option<SmallVec<C>>, Error>
where
    C: Array,
    T: Subst<C::Item>,
{
    let mut result = SmallVec::<C>::new();
    for (i, elem) in this.iter().enumerate() {
        if let Some(subst) = elem.subst(ctx)? {
            if result.is_empty() {
                result.reserve(this.len());
            }
            while i > result.len() {
                debug_assert!(!trip);
                result.push(this[result.len()].shallow_consed(ctx.cons_ctx()))
            }
            trip = true;
            result.push(subst)
        } else if !result.is_empty() || trip {
            if result.is_empty() {
                result.reserve(this.len());
            }
            result.push(elem.shallow_consed(ctx.cons_ctx()))
        }
    }
    Ok(if result.len() == this.len() {
        if result.is_empty() {
            None
        } else {
            Some(result)
        }
    } else {
        None
    })
}

impl<A, C> Subst<SmallVec<C>> for SmallVec<A>
where
    A: Array,
    C: Array,
    A::Item: Subst<C::Item>,
{
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<SmallVec<C>>, Error> {
        subst_small_helper(self, false, ctx)
    }
}

/// Generate implementations of `Subst<Term>` and `Subst<TermId>` from a `Subst<V>` implementation, where `V: Value`
#[macro_export]
macro_rules! value_subst {
    ($value:ty) => {
        impl Subst<Term> for $value {
            fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Term>, Error> {
                let v: Option<$value> = self.subst(ctx)?;
                if let Some(v) = v {
                    Ok(Some(v.into_term()))
                } else {
                    Ok(None)
                }
            }
        }

        impl Subst<TermId> for $value {
            fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<TermId>, Error> {
                let v: Option<$value> = self.subst(ctx)?;
                if let Some(v) = v {
                    Ok(Some(TermId::shallow_cons(v.into_term(), ctx.cons_ctx())))
                } else {
                    Ok(None)
                }
            }
        }
    };
}

/// Generate and use a collection of values, substituted within a given context, or return a failure value if all said values are nil-substituted
#[macro_export]
macro_rules! do_subst {
    (in $ctx:ident; $(let $c:ident$(: $C:ty)? = $e:expr;)* else $f:expr; $r:expr) => {{
        loop {
            $(
                let $c = $e.subst($ctx);
                let $c = match $c {
                Ok(Some(c)) => Ok(c),
                Ok(None) => Err(&$e),
                Err(err) => break Err(err)
            };)*
            break if false $(|| $c.is_ok())* {
                $(let $c$(: $C)? = match $c {
                    Ok(c) => c,
                    Err(c) => (*c).shallow_consed($ctx.cons_ctx())
                };)*
                $r
            } else {
                $f
            }
        }
    }};
    (in $ctx:ident; $(let $c:ident$(: $C:ty)? = $e:expr;)* $r:expr) => {{
        do_subst!(in $ctx; $(let $c$(: $C)? = $e;)* else Ok(None); $r)
    }}
}
