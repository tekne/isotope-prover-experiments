/*!
Mutually recursive definitions

Termination checking implemented using the algorithm from [foetus](https://www.cs.cmu.edu/~abel/foetus.ps).
*/

use isotope_termck::{DecreasingArgs, DefinitionGraph};

use super::*;

/// A mutually recursive definition
#[derive(Clone, PartialEq, Eq)]
pub struct RecDef {
    /// The code of this recursive definition
    code: Code,
    /// The free-variable set of this recursive definition
    fv: Filter,
    /// The objects being defined
    defs: SmallVec<[TermId; 2]>,
    /// The decreasing arguments of this definition
    args: DecreasingArgs,
    /// The flags for this definition
    flags: Flags,
}

impl Debug for RecDef {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_tuple("RecDef").field(&self.defs).finish()
    }
}

impl RecDef {
    /// Attempt to create a mutually-recursive definition from a vector of definitions
    /// TODO: explicit decreasing argument annotations...
    pub fn try_new(defs: SmallVec<[TermId; 2]>) -> Result<RecDef, Error> {
        let definition_graph = Self::termination_check(&defs)?;
        //TODO: argument factorization...
        let decreasing_args = definition_graph.definitions.into_nodes_edges().0;
        debug_assert!(decreasing_args.len() <= defs.len());
        let mut args = DecreasingArgs::new(defs.len());
        let mut args_defined = 0;
        for decreasing_args in decreasing_args {
            let decreasing_args = decreasing_args.weight.ok_or(Error::Nonterminating)?;
            for component_decreasing_args in decreasing_args.iter() {
                debug_assert!(args_defined <= defs.len());
                for &arg in component_decreasing_args {
                    args.push(args_defined, arg)
                }
                args_defined += 1;
            }
        }
        let fv = Self::compute_fv(&defs);
        let flags = Self::compute_flags(&defs, fv);
        let code = Self::compute_code(&defs);
        Ok(RecDef {
            defs,
            fv,
            code,
            args,
            flags,
        })
    }

    /// Perform a termination check on a vector of mutually recursive definitions
    #[inline]
    pub fn termination_check(defs: &[TermId]) -> Result<DefinitionGraph, Error> {
        crate::termination::make_def_graph(defs)
    }

    /// Compute the free variable set for an inductive definition having a vector of families
    pub fn compute_fv(defs: &[TermId]) -> Filter {
        let mut result = Filter::EMPTY;
        for def in defs {
            result = result.union(def.shift_down_fv(1))
        }
        result
    }
    /// Compute the flags for an inductive definition having a vector of families
    pub fn compute_flags(defs: &[TermId], fv: Filter) -> Flags {
        let mut flags = Flags::default();
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(Flags::TYCK_NIL, defs.iter().all(|def| def.nil_tyck()));
        flags
    }

    /// Compute the code for an inductive definition having a vector of families
    pub fn compute_code(defs: &[TermId]) -> Code {
        let mut hasher = AHasher::new_with_keys(58478375, 235321);
        for def in defs {
            def.code().hash(&mut hasher);
        }
        Code(hasher.finish())
    }

    /// Get the null-typed code of a given recursive object
    pub fn rec_code(&self, ix: VarIx) -> Result<Code, Error> {
        Ok(Rec::compute_code(self.code, ix, None))
    }

    /// Get the unfixed term for left-addition
    #[inline]
    pub fn left_add_unfixed(ctx: &mut impl ConsCtx) -> TermId {
        let nats = TermId::nats_in(ctx);
        let nat_zero = nats.var_in(0, ctx).unwrap();
        let nat_one = nats.var_in(1, ctx).unwrap();
        let zero_branch = nat_zero.clone();
        let succ = nats.con_in(1, ctx).unwrap();
        let recursive_call = nats
            .binary_in(ctx)
            .unwrap()
            .var_rec_in(3, 0, ctx)
            .unwrap()
            .app_in(nat_zero, ctx)
            .unwrap()
            .app_in(nat_one.clone(), ctx)
            .unwrap();
        let succ_branch = succ
            .app_in(recursive_call, ctx)
            .unwrap()
            .abs_in(nats.clone(), ctx)
            .unwrap();
        let case = nats
            .case_in(nats.const_over_in(nats.clone(), ctx).unwrap(), ctx)
            .unwrap()
            .app_in(zero_branch, ctx)
            .unwrap()
            .app_in(succ_branch, ctx)
            .unwrap()
            .app_in(nat_one, ctx)
            .unwrap();
        let fixpoint = case
            .abs_in(nats.clone(), ctx)
            .unwrap()
            .abs_in(nats, ctx)
            .unwrap();
        fixpoint
    }

    /// Get left-addition as a recursive definition
    #[inline]
    pub fn left_add(ctx: &mut impl ConsCtx) -> RecDef {
        RecDef::try_new(smallvec![RecDef::left_add_unfixed(ctx)])
            .expect("Left-addition is a valid fixpoint")
    }

    /// Get the unfixed term for left-multiplication
    #[inline]
    pub fn left_mul_unfixed(ctx: &mut impl ConsCtx) -> TermId {
        //TODO: optimize...
        let nats = TermId::nats_in(ctx);
        let add = TermId::left_add_in(ctx);
        let nat_zero = nats.var_in(0, ctx).unwrap();
        let nat_one = nats.var_in(1, ctx).unwrap();
        let zero = nats.con_in(0, ctx).unwrap();
        let zero_branch = zero;
        let recursive_call = nats
            .binary_in(ctx)
            .unwrap()
            .var_rec_in(3, 0, ctx)
            .unwrap()
            .app_in(nat_zero, ctx)
            .unwrap()
            .app_in(nat_one.clone(), ctx)
            .unwrap();
        let succ_branch = add
            .app_in(nat_one.clone(), ctx)
            .unwrap()
            .app_in(recursive_call, ctx)
            .unwrap()
            .abs_in(nats.clone(), ctx)
            .unwrap();
        let case = nats
            .case_in(nats.const_over_in(nats.clone(), ctx).unwrap(), ctx)
            .unwrap()
            .app_in(zero_branch, ctx)
            .unwrap()
            .app_in(succ_branch, ctx)
            .unwrap()
            .app_in(nat_one, ctx)
            .unwrap();
        let fixpoint = case
            .abs_in(nats.clone(), ctx)
            .unwrap()
            .abs_in(nats, ctx)
            .unwrap();
        fixpoint
    }

    /// Get left-multiplication as a recursive definition
    #[inline]
    pub fn left_mul(ctx: &mut impl ConsCtx) -> RecDef {
        RecDef::try_new(smallvec![RecDef::left_mul_unfixed(ctx)])
            .expect("Left-addition is a valid fixpoint")
    }
}

/// A recursively defined term
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Rec {
    /// This term's code
    code: Code,
    /// This term's free variable set
    fv: Filter,
    /// The surface type of this term
    ty: TermId,
    /// The base type of this term
    base_ty: TermId,
    /// The underlying set of mutually recursive definitions
    def: TermId,
    /// The index of this term's definition
    ix: VarIx,
    /// This definition's flags
    flags: Flags,
}

impl Debug for Rec {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut dbg = f.debug_struct("Rec");
        let code = self.def.code();
        dbg.field(
            "def",
            if let Term::Var(def) = &*self.def {
                def
            } else {
                &code
            },
        )
        .field("ix", &self.ix);
        if self.ty == self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl Rec {
    /// Create a new recursively defined term from a definition and index
    #[inline]
    pub fn try_new(
        def: TermId,
        ix: VarIx,
        base_ty: Option<TermId>,
        ty: Option<TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<Rec, Error> {
        let code = Self::compute_code(def.code(), ix, ty.as_ref());
        let base_ty = Self::compute_base_ty(&def, ix, base_ty.as_ref(), ctx)?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&def, ix, &ty);
        let flags = Self::compute_flags(&def, ix, &ty, &base_ty, fv);
        Ok(Rec {
            code,
            fv,
            ty,
            base_ty,
            def,
            ix,
            flags,
        })
    }

    /// Compute the base type for a recursively defined term
    #[inline]
    pub fn compute_base_ty(
        def_id: &TermId,
        ix: VarIx,
        base_ty: Option<&TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        match &**def_id {
            Term::RecDef(def) => def.defs[ix as usize]
                .ty()
                .ok_or(Error::CannotInfer)?
                .shifted(-1, 0, ctx),
            Term::Var(v) if v.ty().is_none() => {
                if let Some(base_ty) = base_ty {
                    Ok(base_ty.clone())
                } else {
                    Err(Error::CannotInfer)
                }
            }
            _ => Err(Error::ExpectedRecDef),
        }
    }

    /// Compute the code for a recursively defined term
    #[inline]
    pub fn compute_code(def: Code, ix: VarIx, ty: Option<&TermId>) -> Code {
        let mut state = AHasher::new_with_keys(57858283, 86542348143);
        def.hash(&mut state);
        ix.hash(&mut state);
        ty.hash(&mut state);
        Code(state.finish())
    }

    /// Compute the free variable set for a recursively defined term
    #[inline]
    pub fn compute_fv(def: &TermId, _ix: VarIx, ty: &TermId) -> Filter {
        def.fv().union(ty.fv())
    }

    /// Compute the flags for a recursively defined term
    #[inline]
    pub fn compute_flags(
        def: &TermId,
        _ix: VarIx,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        flags.set(Flags::IS_TYPE, ty.is_ty() || base_ty.is_ty());
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            def.nil_tyck() && ty.nil_tyck() && ty == base_ty,
        );
        flags
    }

    /// Get this term's definition
    #[inline]
    pub fn def(&self) -> &TermId {
        &self.def
    }

    /// Get the base type of this term
    #[inline]
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this term
    #[inline]
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    /// Get the index of this term's definition
    #[inline]
    pub fn ix(&self) -> VarIx {
        self.ix
    }

    /// Get whether this value is a recursive definition variable
    #[inline]
    pub fn is_rec_def_var(&self, var: VarIx) -> bool {
        match &*self.def {
            Term::Var(def) => def.ix() == var,
            _ => false,
        }
    }

    /// Get the decreasing argument-set of this recursive definition, or `None` if it should never be reduced (i.e., in the case of a variable or, maybe later, nonterminating function)
    #[inline]
    pub fn dec_args(&self) -> Option<&[usize]> {
        self.def.as_rec_def().ok()?.args.get(self.ix as usize)
    }
}

impl Equiv<RecDef> for RecDef {
    fn term_eq_mut(
        &self,
        other: &RecDef,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        self.defs.term_eq_mut(&other.defs, fail_early, ctx)
    }
}

impl Cons<RecDef> for RecDef {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<RecDef> {
        do_cons! {
            in ctx;
            let defs = self.defs;
            Some(RecDef {
                defs,
                args: self.args.clone(),
                ..*self
            })
        }
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> RecDef {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> RecDef {
        self.clone()
    }
}

impl Cast<RecDef> for RecDef {
    fn cast(&self, _ty: TermRef, _ctx: &mut impl ConsCtx) -> Result<Option<RecDef>, Error> {
        Err(Error::UntypedCast)
    }
}

impl Subst<RecDef> for RecDef {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<RecDef>, Error> {
        ctx.push_param(None)?;
        let defs = self.defs.subst(ctx);
        ctx.pop_param()?;
        if let Some(defs) = defs? {
            //TODO: think about this...
            let fv = RecDef::compute_fv(&defs[..]);
            let flags = RecDef::compute_flags(&defs, fv);
            let code = RecDef::compute_code(&defs[..]);
            Ok(Some(RecDef {
                defs,
                fv,
                code,
                args: self.args.clone(),
                flags,
            }))
        } else {
            Ok(None)
        }
    }
}

eq_term!(RecDef);
value_impl!(RecDef);

impl Value for RecDef {
    #[inline]
    fn into_term(self) -> Term {
        Term::RecDef(self)
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        let mut failure = false;
        for def in self.defs.iter() {
            match def.tyck_mut(fail_early, ctx) {
                Some(true) => {}
                Some(false) => return Some(false),
                None => {
                    failure = true;
                    if fail_early {
                        break;
                    }
                }
            }
        }
        if failure {
            None
        } else {
            Some(true)
        }
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            let def_shift = self.defs.len() as VarIx;
            let sh_equiv = equiv.wrapping_add(def_shift);
            let sh_min = if let Some(min) = min.checked_add(def_shift as VarIx) {
                min
            } else {
                return false;
            };
            let sh_max = max.saturating_add(def_shift as VarIx);
            for def in &self.defs {
                if def.var_within(sh_equiv, sh_min, sh_max) {
                    return true;
                }
            }
            false
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        for def in &self.defs {
            def.visit_transitive_deps_rec(shift + 1, visitor)?
        }
        Ok(())
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        for def in &self.defs {
            trans(hasher, def)
        }
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        if self.defs.len() != other.defs.len() {
            return self.defs.cmp(&other.defs);
        }
        let mut ord = Ordering::Equal;
        for (i, d) in self.defs.iter().enumerate() {
            ord = ord.then(d.cmp_term(&other.defs[i]));
            if ord != Ordering::Equal {
                break;
            }
        }
        ord
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

impl Equiv<Rec> for Rec {
    fn term_eq_mut(&self, other: &Rec, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        opt_and! {
            fail_early;
            self.ty().term_eq_mut(&other.ty(), fail_early, ctx),
            if self.ix == other.ix { Some(true) } else { None },
            if let Some(true) = self.def.term_eq_mut(&other.def, fail_early, ctx) { Some(true) } else { None }
        }
    }
}

impl Cons<Rec> for Rec {
    #[inline]
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<Rec> {
        do_cons! {
            in ctx;
            let ty = self.ty;
            let base_ty = self.base_ty;
            let def = self.def;
            Some(Rec{ ty, base_ty, def, ..*self })
        }
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> Rec {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    #[inline]
    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> Rec {
        self.clone()
    }
}

impl Cast<Rec> for Rec {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<Rec>, Error> {
        if self.ty().unwrap() == ty {
            Ok(None)
        } else {
            //FIXME
            Rec::try_new(
                self.def.consed(ctx),
                self.ix,
                Some(self.base_ty.clone()),
                Some(ty.into_owned()),
                ctx.cons_ctx(),
            )
            .map(Some)
        }
    }
}

impl Subst<Rec> for Rec {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Rec>, Error> {
        do_subst! {
            in ctx;
            let ty = self.ty;
            let base_ty = self.base_ty;
            let def = self.def;
            //FIXME: this
            Rec::try_new(def, self.ix, Some(base_ty), Some(ty), ctx.cons_ctx()).map(Some)
        }
    }
}

eq_term!(Rec);
value_impl!(Rec);

impl Value for Rec {
    #[inline]
    fn into_term(self) -> Term {
        Term::Rec(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.get_ty().borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.base_ty.borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.ty, &self.base_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.def.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        self.term_eq_mut(other, true, &mut Structural(true))
            .unwrap_or(false)
    }

    #[inline]
    fn is_ty(&self) -> bool {
        self.get_ty().is_universe()
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        self.ty.var_within(equiv, min, max) || self.def.var_within(equiv, min, max)
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        false
    }

    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        //TODO: merge steps?
        if let Ok(rec_def) = self.def.as_rec_def() {
            let mut unfold = false;
            //TODO: check whether context forces unfolding?
            //TODO: check whether context *allows* unfolding? up to what depth?
            if !unfold {
                let dec_args = rec_def.args.get(self.ix as usize).unwrap();
                //TODO: clean up, and think about this...
                if dec_args.is_empty() {
                    unfold = true
                } else {
                    for &ix in dec_args {
                        if let Some(arg) = args.arg(ix) {
                            if let Ok(_cons) = arg.get_root().as_ind_cons() {
                                unfold = true;
                                break;
                            }
                        }
                    }
                }
            }
            if unfold {
                let def = self.def.subst_cons(ctx)?;
                ctx.push_subst(def, None, false)?;
                let result = rec_def.defs[self.ix as usize].apply(args, ctx);
                ctx.pop_subst(false)?;
                return result;
            }
        }
        self.subst(ctx)
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.def.visit_transitive_deps_rec(shift, visitor)
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        trans(hasher, &self.ty);
        self.ix.hash(hasher);
        trans(hasher, &self.def)
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then(self.ix.cmp(&other.ix))
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.def.cmp(&other.def))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn trivial_recursive_identity() {
        let mut ctx = HashCtx::default();
        let nats = TermId::nats_in(&mut ctx);
        let zero = nats.con_in(0, &mut ctx).unwrap();
        let succ = nats.con_in(1, &mut ctx).unwrap();
        let one = succ.app_in(zero.clone(), &mut ctx).unwrap();
        let two = succ.app_in(one.clone(), &mut ctx).unwrap();
        let id = nats.id_fn_in(&mut ctx).unwrap();
        let rid_def = RecDef::try_new(smallvec![id]).unwrap().into_id_in(&mut ctx);
        let rid = rid_def.rec_in(0, &mut ctx).unwrap();
        assert_eq!(rid.dec_args().unwrap(), Some(&[][..]));
        let mut ctx = SubstStack::new(&mut ctx);
        assert_eq!(
            rid.app_in(zero.clone(), ctx.cons_ctx())
                .unwrap()
                .norm_cons_in(&mut 1000, &mut ctx)
                .unwrap(),
            zero
        );
        assert_eq!(
            rid.app_in(one.clone(), ctx.cons_ctx())
                .unwrap()
                .norm_cons_in(&mut 1000, &mut ctx)
                .unwrap(),
            one
        );
        assert_eq!(
            rid.app_in(two.clone(), ctx.cons_ctx())
                .unwrap()
                .norm_cons_in(&mut 1000, &mut ctx)
                .unwrap(),
            two
        );
    }

    #[test]
    fn recursive_identity_nats() {
        let mut ctx = HashCtx::default();
        let nats = TermId::nats_in(&mut ctx);
        let unary = nats.unary_in(&mut ctx).unwrap();
        let zero = nats.con_in(0, &mut ctx).unwrap();
        let succ = nats.con_in(1, &mut ctx).unwrap();
        let one = succ.app_in(zero.clone(), &mut ctx).unwrap();
        let two = succ.app_in(one.clone(), &mut ctx).unwrap();
        let nats_0 = nats.var_in(0, &mut ctx).unwrap();
        let succ_branch = Lambda::try_new(
            nats.clone(),
            succ.app_in(
                unary
                    .var_rec_in(2, 0, &mut ctx)
                    .unwrap()
                    .app_in(nats_0.clone(), &mut ctx)
                    .unwrap(),
                &mut ctx,
            )
            .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        let rid_result = Case::try_new(
            nats.clone(),
            nats.const_over_in(nats.clone(), &mut ctx).unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx)
        .app_in(zero.clone(), &mut ctx)
        .unwrap()
        .app_in(succ_branch.clone(), &mut ctx)
        .unwrap()
        .app_in(nats_0, &mut ctx)
        .unwrap();
        let rid_fn = Lambda::try_new(nats, rid_result, None, &mut ctx)
            .unwrap()
            .into_id_in(&mut ctx);
        let rid_def = RecDef::try_new(smallvec![rid_fn])
            .unwrap()
            .into_id_in(&mut ctx);
        let rid = rid_def.rec_in(0, &mut ctx).unwrap();
        assert_eq!(rid.dec_args().unwrap(), Some(&[0][..]));

        let mut ctx = SubstStack::new(&mut ctx);
        assert_eq!(
            rid.app_in(zero.clone(), ctx.cons_ctx())
                .unwrap()
                .norm_cons_in(&mut 10, &mut ctx)
                .unwrap(),
            zero
        );
        assert_eq!(
            rid.app_in(one.clone(), ctx.cons_ctx())
                .unwrap()
                .norm_cons_in(&mut 10, &mut ctx)
                .unwrap(),
            one
        );
        assert_eq!(
            rid.app_in(two.clone(), ctx.cons_ctx())
                .unwrap()
                .norm_cons_in(&mut 10, &mut ctx)
                .unwrap(),
            two
        );
    }

    #[test]
    fn left_add() {
        let mut ctx = HashCtx::default();
        let nats = TermId::nats_in(&mut ctx);
        let zero = nats.con_in(0, &mut ctx).unwrap();
        let succ = nats.con_in(1, &mut ctx).unwrap();
        let one = succ.app_in(zero.clone(), &mut ctx).unwrap();
        let two = succ.app_in(one.clone(), &mut ctx).unwrap();
        let three = succ.app_in(two.clone(), &mut ctx).unwrap();
        let four = succ.app_in(three.clone(), &mut ctx).unwrap();
        let num = [zero, one, two, three, four];
        let add = TermId::left_add_in(&mut ctx);
        assert_eq!(add, *LEFT_ADD);
        let mut ctx = SubstStack::new(&mut ctx);
        for i in 0..=4 {
            for j in 0..=(4 - i) {
                let aij = add
                    .app_in(num[i].clone(), ctx.cons_ctx())
                    .unwrap()
                    .app_in(num[j].clone(), ctx.cons_ctx())
                    .unwrap()
                    .norm_cons_in(&mut 200, &mut ctx)
                    .unwrap();
                assert_eq!(aij, num[i + j]);
            }
        }
    }

    #[test]
    fn left_mul() {
        let nats = TermId::nats();
        let zero = nats.con(0).unwrap();
        let succ = nats.con(1).unwrap();
        let one = succ.app(zero.clone()).unwrap();
        let two = succ.app(one.clone()).unwrap();
        let three = succ.app(two.clone()).unwrap();
        let four = succ.app(three.clone()).unwrap();
        let num = [zero, one, two, three, four];
        let mul = TermId::left_mul();
        for i in 0..=4 {
            for j in 0..=(if i == 0 { 4 } else { 4 / i }) {
                assert_eq!(
                    mul.app(num[i].clone())
                        .unwrap()
                        .app(num[j].clone())
                        .unwrap()
                        .norm(&mut 200)
                        .unwrap(),
                    num[i * j],
                );
            }
        }
    }
}
