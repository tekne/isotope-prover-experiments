/*!
Variables
*/
use super::*;

/// A variable, represented by a de-Bruijn index
#[derive(Clone)]
pub struct Var {
    /// The type of this variable
    ty: Option<TermId>,
    /// The base type of this variable
    base_ty: Option<TermId>,
    /// The index of this variable
    ix: VarIx,
    /// The free variable set of this variable definition
    fv: Filter,
    /// This variable definition's code
    code: Code,
    /// The flags of this variable
    flags: Flags,
}

impl Var {
    /// Create a new variable, with an optional type annotation
    #[inline]
    pub fn try_new(ix: VarIx, base_ty: Option<TermId>, ty: Option<TermId>) -> Result<Var, Error> {
        if let Some(base_ty) = &base_ty {
            if !base_ty.is_ty() {
                return Err(Error::ExpectedType);
            }
            if let Some(ty) = &ty {
                if !ty.is_ty() {
                    return Err(Error::ExpectedType);
                }
            }
        } else {
            if ty.is_some() {
                //TODO: proper code
                return Err(Error::Invariant);
            }
        }
        let ty = ty.or_else(|| base_ty.clone());
        let fv = Self::compute_fv(ix, base_ty.as_ref(), ty.as_ref())?;
        let flags = Self::compute_flags(ix, base_ty.as_ref(), ty.as_ref(), fv);
        let code = Self::compute_code(ix, base_ty.as_ref(), ty.as_ref());
        Ok(Var {
            base_ty,
            ty,
            ix,
            fv,
            code,
            flags,
        })
    }

    /// Create a new variable with a given type
    #[inline]
    pub fn with_ty(ix: VarIx, ty: TermId) -> Result<Var, Error> {
        Self::try_new(ix, Some(ty), None)
    }

    /// Create a new, untyped variable with a given index
    #[inline]
    pub fn with_ix(ix: VarIx) -> Var {
        let fv = Self::compute_fv(ix, None, None).unwrap();
        let code = Self::compute_code(ix, None, None);
        let flags = Self::compute_flags(ix, None, None, fv);
        Var {
            ty: None,
            base_ty: None,
            ix,
            fv,
            code,
            flags,
        }
    }

    /// Compute the free variable set of a variable with a given index and (optional) type
    #[inline]
    pub fn compute_fv(
        ix: VarIx,
        base_ty: Option<&TermId>,
        ty: Option<&TermId>,
    ) -> Result<Filter, Error> {
        //TODO: cyclic variable check? Or does lambda cover it?
        Ok(base_ty.fv().insert(ix).union(ty.fv()))
    }

    /// Compute the flags of a variable with a given index and (optional) type
    #[inline]
    pub fn compute_flags(
        _ix: VarIx,
        base_ty: Option<&TermId>,
        ty: Option<&TermId>,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        if let (Some(base_ty), Some(ty)) = (base_ty, ty) {
            flags.set(Flags::IS_IRR, base_ty.is_prop() || ty.is_prop());
            flags.set(Flags::IS_TYPE, base_ty.is_universe() || ty.is_universe());
            flags.set(Flags::IS_PROP, base_ty.is_prop_universe() || ty.is_prop_universe());
        }
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(Flags::TYCK_NIL, ty.tyck_in_empty() && ty == base_ty);
        flags
    }

    /// Compute the code of a variable with a given index and (optional) type
    #[inline]
    pub fn compute_code(ix: VarIx, base_ty: Option<&TermId>, ty: Option<&TermId>) -> Code {
        let mut hasher = AHasher::new_with_keys(0x74759823, 0x48427463);
        ix.hash(&mut hasher);
        base_ty.hash(&mut hasher);
        ty.hash(&mut hasher);
        Code(hasher.finish())
    }

    /// Get the index of this variable
    #[inline]
    #[cfg(not(tarpaulin_include))]
    pub fn ix(&self) -> VarIx {
        self.ix
    }
}

impl PartialEq for Var {
    fn eq(&self, other: &Self) -> bool {
        self.code == other.code
            && self.ix == other.ix
            && self.base_ty == other.base_ty
            && self.ty == other.ty
    }
}

impl Eq for Var {}

self_cons! {
    Var;
    this, ctx => {
        let ty = this.ty.cons(ctx)?;
        let base_ty = this.base_ty.cons(ctx)?;
        Some(Var { ty, base_ty, ..*this })
    }
}

value_cons!(Var);

impl Cast<Var> for Var {
    fn cast(&self, ty: TermRef, _ctx: &mut impl ConsCtx) -> Result<Option<Var>, Error> {
        if Some(&ty) == self.ty().as_ref() {
            return Ok(None);
        }
        //TODO: fixme
        Var::try_new(self.ix, Some(ty.into_owned()), None).map(Some)
    }
}

value_cast!(Var);

impl Equiv<Var> for Var {
    fn term_eq_mut(&self, other: &Var, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            Some(self.ix == other.ix),
            self.ty().term_eq_mut(&other.ty(), fail_early, ctx),
            self.base_ty().term_eq_mut(&other.base_ty(), fail_early, ctx)
        )
    }
}

eq_term!(Var);
eq_termid!(Var);

impl Subst<TermId> for Var {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<TermId>, Error> {
        ctx.subst_ix(self.ix, self.base_ty.as_ref(), self.ty.as_ref())
    }
}

impl Value for Var {
    #[inline]
    fn into_term(self) -> Term {
        Term::Var(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        self.ty.as_ref().map(TermRef::borrow_id).or(self.base_ty())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        self.base_ty.as_ref().map(TermRef::borrow_id)
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            match (self.ty(), self.base_ty()) {
                (Some(ty), Some(base_ty)) => {
                    ctx.constrain_mut(&ty, &base_ty, fail_early)
                },
                (ty, base_ty) => {
                    debug_assert_eq!(ty, None);
                    debug_assert_eq!(base_ty, None);
                    Some(true)
                }
            },
            self.ty.tyck_mut(fail_early, ctx),
            self.base_ty.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        //TODO: improve?
        match other {
            Term::Var(other) => self.ix == other.ix && self.base_ty == other.base_ty,
            _ => false,
        }
    }

    #[inline]
    fn is_universe(&self) -> bool {
        false
    }

    #[inline]
    fn is_ty(&self) -> bool {
        if let Some(ty) = self.ty() {
            ty.is_universe()
        } else {
            false
        }
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if self.ix % Filter::BITS == equiv % Filter::BITS && self.ix >= min && self.ix <= max {
            true
        } else if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.base_ty.var_within(equiv, min, max) || self.ty.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        true
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        if let Some(base_ty) = &self.base_ty {
            visitor(true, shift, base_ty.borrow_id())?;
        }
        if let Some(ty) = &self.ty {
            visitor(true, shift, ty.borrow_id())?;
        }
        Ok(())
    }

    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        self.ix.hash(hasher);
        if let Some(ty) = &self.ty() {
            trans(hasher, ty)
        } else {
            0x568957.hash(hasher)
        }
        if let Some(ty) = &self.base_ty() {
            trans(hasher, ty)
        } else {
            0x5576246.hash(hasher)
        }
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then_with(|| self.base_ty.cmp(&other.base_ty))
            .then_with(|| self.ty.cmp(&other.ty))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

impl Debug for Var {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut dbg = f.debug_tuple("Var");
        dbg.field(&self.ix).field(&self.base_ty);
        if let Some(ty) = &self.ty {
            dbg.field(ty);
        }
        dbg.finish()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn var_subst_slice() {
        use std::slice::from_ref;
        let mut ctx = HashCtx::default();
        let set = Universe::set().into_id_in(&mut ctx);
        let a = Var::with_ty(0, set.clone()).unwrap().into_id_in(&mut ctx);
        let b = Var::with_ty(1, set.clone()).unwrap().into_id_in(&mut ctx);
        for &r in &[&a, &b, &set] {
            assert_eq!(
                a.subst_slice(from_ref(&r), 0, &mut ctx).unwrap().unwrap(),
                *r
            );
            assert_eq!(a.subst_slice(from_ref(&r), 1, &mut ctx), Ok(None));
        }
    }
}
