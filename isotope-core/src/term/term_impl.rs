/*!
Implementations for `TermId` and `Term`, to avoid cluttering `mod.rs`
*/
use std::{borrow::Borrow, cmp::Ordering};

use indexmap::Equivalent;

use super::*;

impl Cons<TermId> for TermRef<'_> {
    #[inline]
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<TermId> {
        if let Some(term) = ctx.try_cons(&*self.0) {
            if term.ptr_eq(self) {
                None
            } else {
                Some(term.clone())
            }
        } else if let Some(id) = self.0.cons(ctx) {
            Some(id)
        } else {
            Some(
                ctx.shallow_cons(self.borrow_id())
                    .unwrap_or_else(|| self.clone_into_owned()),
            )
        }
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> TermId {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone_into_owned()
        }
    }

    #[inline]
    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> TermId {
        self.clone_into_owned()
    }
}

impl Cons<Term> for TermRef<'_> {
    #[inline]
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<Term> {
        self.0.cons(ctx)
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> Term {
        self.0.consed(ctx)
    }

    #[inline]
    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> Term {
        self.0.shallow_consed(ctx)
    }
}

impl Subst<TermId> for TermRef<'_> {
    #[inline]
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<TermId>, Error> {
        self.0.subst(ctx)
    }
}

impl Cast<Term> for TermRef<'_> {
    #[inline]
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<Term>, Error> {
        self.0.cast(ty, ctx)
    }
}

impl Cast<TermId> for TermRef<'_> {
    #[inline]
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<TermId>, Error> {
        self.0.cast(ty, ctx)
    }
}

impl Equiv<Term> for TermRef<'_> {
    #[inline]
    fn term_eq_mut(&self, other: &Term, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        self.0.term_eq_mut(other, fail_early, ctx)
    }
}

equiv_termid!(TermRef<'_>);

impl Value for TermRef<'_> {
    #[inline]
    fn into_term(self) -> Term {
        (*self.0).clone()
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        self.0.ty()
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        self.0.base_ty()
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        if let Ok(result) = ctx.try_tyck_mut(self, fail_early) {
            return result;
        }
        let result = self.0.tyck_mut(fail_early, ctx);
        //TODO: this
        // ctx.register_tyck(self.borrow_id(), result);
        result
    }

    fn tyck(&self, fail_early: bool, mut ctx: &impl TyCtx) -> Option<bool> {
        self.tyck_mut(fail_early, &mut ctx)
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        self.0.is_subty(other)
    }

    #[inline]
    fn join_ty(&self, other: TermRef<'_>, ctx: &mut impl ConsCtx) -> Result<Binary<TermId>, Error> {
        self.0.join_ty(other, ctx)
    }

    #[inline]
    fn is_universe(&self) -> bool {
        self.0.is_universe()
    }

    #[inline]
    fn is_prop_universe(&self) -> bool {
        self.0.is_prop_universe()
    }

    #[inline]
    fn is_ty(&self) -> bool {
        self.0.is_ty()
    }

    #[inline]
    fn nil_tyck(&self) -> bool {
        self.0.nil_tyck()
    }

    #[inline]
    fn is_closed(&self) -> bool {
        self.0.is_closed()
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        self.0.var_within(equiv, min, max)
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.0.fv()
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.0.flags()
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        self.0.is_unique_head()
    }

    #[inline]
    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        self.0.apply(args, ctx)
    }

    #[inline]
    fn apply_ty(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        self.0.apply_ty(args, ctx)
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        self.0.app_root()
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        self.0.visit_transitive_deps(shift, visitor)
    }

    #[inline]
    fn visit_direct_deps<E>(
        &self,
        shift: VarIx,
        visitor: impl FnMut(bool, VarIx, TermRef) -> Result<(), E>,
    ) -> Result<(), E> {
        self.0.visit_direct_deps(shift, visitor)
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        self.0.hash_deps(hasher, trans)
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        (*self.0).cmp(&*other.0)
    }

    #[inline]
    fn code(&self) -> Code {
        self.0.code()
    }

    #[inline]
    fn into_id(self) -> TermId {
        self.into_owned()
    }

    #[inline]
    fn into_id_in(self, ctx: &mut impl ConsCtx) -> TermId {
        ctx.shallow_cons(self.borrow_id())
            .unwrap_or_else(|| self.into_owned())
    }

    #[inline]
    fn clone_into_term(&self) -> Term {
        (*self.0).clone()
    }

    #[inline]
    fn clone_into_id(&self) -> TermId {
        self.clone_into_owned()
    }

    #[inline]
    fn clone_into_id_in(&self, ctx: &mut impl ConsCtx) -> TermId {
        ctx.shallow_cons(self.borrow_id())
            .unwrap_or_else(|| self.clone_into_owned())
    }

    #[inline]
    fn has_dep(&self, var: VarIx) -> bool {
        self.0.has_dep(var)
    }
}

impl Deref for TermRef<'_> {
    type Target = Term;

    #[inline]
    fn deref(&self) -> &Self::Target {
        self.0.deref()
    }
}

impl Cons<Term> for Term {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<Term> {
        for_term!(self; t => t.cons(ctx))
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> Term {
        for_term!(self; t => t.consed(ctx))
    }

    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> Term {
        self.clone()
    }
}

impl Cons<TermId> for Term {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<TermId> {
        for_term!(self; t => t.cons(ctx))
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> TermId {
        for_term!(self; t => t.consed(ctx))
    }

    fn shallow_consed(&self, ctx: &mut impl ConsCtx) -> TermId {
        TermId::shallow_cons(self.clone(), ctx)
    }
}

impl Cast<Term> for Term {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<Term>, Error> {
        for_term!(self; t => t.cast(ty, ctx))
    }
}

impl Cast<TermId> for Term {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.cast(ty, ctx))
    }
}

impl Subst<TermId> for Term {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<TermId>, Error> {
        if let Some(result) = ctx.try_subst(self)? {
            Ok(result)
        } else {
            let result = for_term!(self; t => t.subst(ctx)?);
            ctx.cache_subst(self, result.as_ref())?;
            Ok(result)
        }
    }
}

impl Equiv<Term> for Term {
    fn term_eq_mut(&self, other: &Term, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        if let Ok(result) = ctx.try_eq_mut(self, other, fail_early) {
            return result;
        }
        for_term!(self; t => t.term_eq_mut(other, fail_early, ctx))
    }
}

eq_termid!(Term);

impl Value for Term {
    #[inline]
    fn into_term(self) -> Term {
        self
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        for_term!(self; t => t.ty())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        for_term!(self; t => t.base_ty())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        if let Ok(result) = ctx.try_tyck_mut(self, fail_early) {
            return result;
        }
        for_term!(self; t => t.tyck_mut(fail_early, ctx))
    }

    #[inline]
    fn tyck(&self, fail_early: bool, ctx: &impl TyCtx) -> Option<bool> {
        if let Ok(result) = ctx.try_tyck(self, fail_early) {
            return result;
        }
        for_term!(self; t => t.tyck(fail_early, ctx))
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        for_term!(self; t => t.is_subty(other))
    }

    #[inline]
    fn join_ty(&self, other: TermRef<'_>, ctx: &mut impl ConsCtx) -> Result<Binary<TermId>, Error> {
        for_term!(self; t => t.join_ty(other, ctx))
    }

    #[inline]
    fn is_universe(&self) -> bool {
        for_term!(self; t => t.is_universe())
    }

    #[inline]
    fn is_prop_universe(&self) -> bool {
        for_term!(self; t => t.is_prop_universe())
    }

    #[inline]
    fn is_ty(&self) -> bool {
        for_term!(self; t => t.is_ty())
    }

    #[inline]
    fn nil_tyck(&self) -> bool {
        for_term!(self; t => t.nil_tyck())
    }

    #[inline]
    fn is_closed(&self) -> bool {
        for_term!(self; t => t.is_closed())
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        for_term!(self; t => t.var_within(equiv, min, max))
    }

    #[inline]
    fn fv(&self) -> Filter {
        for_term!(self; t => t.fv())
    }

    #[inline]
    fn flags(&self) -> Flags {
        for_term!(self; t => t.flags())
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        for_term!(self; t => t.is_unique_head())
    }

    #[inline]
    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.apply(args, ctx))
    }

    #[inline]
    fn apply_ty(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.apply_ty(args, ctx))
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        for_term!(self; t => t.app_root())
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        for_term!(self; t => t.visit_transitive_deps(shift, visitor))
    }

    #[inline]
    fn visit_direct_deps<E>(
        &self,
        shift: VarIx,
        visitor: impl FnMut(bool, VarIx, TermRef) -> Result<(), E>,
    ) -> Result<(), E> {
        for_term!(self; t => t.visit_direct_deps(shift, visitor))
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        std::mem::discriminant(self).hash(hasher);
        for_term!(self; t => t.hash_deps(hasher, trans))
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        let cmp = self.code().cmp(&other.code());
        if cmp == Ordering::Equal && self as *const _ != other as *const _ {
            for_term_diag!((self, other);
                (l, r) => l.cmp_term(r),
                (Term::Var(_), _) => Ordering::Less,
                (_, Term::Var(_)) => Ordering::Greater,
                (Term::Lambda(_), _) => Ordering::Less,
                (_, Term::Lambda(_)) => Ordering::Greater,
                (Term::App(_), _) => Ordering::Less,
                (_, Term::App(_)) => Ordering::Greater,
                (Term::Pi(_), _) => Ordering::Less,
                (_, Term::Pi(_)) => Ordering::Greater,
                (Term::Universe(_), _) => Ordering::Less,
                (_, Term::Universe(_)) => Ordering::Greater,
                (Term::IndDef(_), _) => Ordering::Less,
                (_, Term::IndDef(_)) => Ordering::Greater,
                (Term::IndFamily(_), _) => Ordering::Less,
                (_, Term::IndFamily(_)) => Ordering::Greater,
                (Term::IndCons(_), _) => Ordering::Less,
                (_, Term::IndCons(_)) => Ordering::Greater,
                (Term::Case(_), _) => Ordering::Less,
                (_, Term::Case(_)) => Ordering::Greater,
                (Term::RecDef(_), _) => Ordering::Less,
                (_, Term::RecDef(_)) => Ordering::Greater
                //Note: `Term::Rec` is already covered, since there is nothing after it in the `Term` enum
            )
        } else {
            cmp
        }
    }

    #[inline]
    fn code(&self) -> Code {
        for_term!(self; t => t.code())
    }

    #[inline]
    fn into_id(self) -> TermId {
        for_term!(self; t => t.into_id())
    }

    #[inline]
    fn into_id_in(self, ctx: &mut impl ConsCtx) -> TermId {
        if let Some(id) = ctx.try_cons(&self) {
            id.clone()
        } else {
            for_term!(self; t => t.into_id_in(ctx))
        }
    }

    #[inline]
    fn clone_into_term(&self) -> Term {
        self.clone()
    }

    #[inline]
    fn clone_into_id(&self) -> TermId {
        for_term!(self; t => t.clone_into_id())
    }

    #[inline]
    fn clone_into_id_in(&self, ctx: &mut impl ConsCtx) -> TermId {
        if let Some(id) = ctx.try_cons(self) {
            id.clone()
        } else {
            // Note: this compiles since `into_id` for `&Value` is `clone_into_id` for `Value`
            for_term!(self; t => t.into_id_in(ctx))
        }
    }

    #[inline]
    fn has_dep(&self, var: VarIx) -> bool {
        for_term!(self; t => t.has_dep(var))
    }
}

impl PartialEq for TermRef<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.ptr_eq(other) || **self == **other
    }
}

impl Eq for TermRef<'_> {}

impl Hash for TermRef<'_> {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        (**self).code().hash(state)
    }
}

impl Hash for Term {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.code().hash(state)
    }
}

impl PartialOrd for TermRef<'_> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some((*self.0).cmp_term(&*other.0))
    }
}

impl Ord for TermRef<'_> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        (*self.0).cmp_term(&*other.0)
    }
}

impl PartialOrd for Term {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp_term(other))
    }
}

impl Ord for Term {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.cmp_term(other)
    }
}

impl Debug for TermRef<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Debug for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        #[cfg(feature = "syntax")]
        {
            write!(f, "{}", self)
        }
        #[cfg(not(feature = "syntax"))]
        {
            for_term!(self; t => t.fmt(f))
        }
    }
}

impl PartialEq for Term {
    fn eq(&self, other: &Self) -> bool {
        if self.code() != other.code() {
            false
        } else if self as *const _ == other as *const _ {
            true
        } else {
            match (self, other) {
                (Self::Var(l0), Self::Var(r0)) => l0 == r0,
                (Self::Lambda(l0), Self::Lambda(r0)) => l0 == r0,
                (Self::App(l0), Self::App(r0)) => l0 == r0,
                (Self::Pi(l0), Self::Pi(r0)) => l0 == r0,
                (Self::Universe(l0), Self::Universe(r0)) => l0 == r0,
                (Self::IndDef(l0), Self::IndDef(r0)) => l0 == r0,
                (Self::IndFamily(l0), Self::IndFamily(r0)) => l0 == r0,
                (Self::IndCons(l0), Self::IndCons(r0)) => l0 == r0,
                (Self::Case(l0), Self::Case(r0)) => l0 == r0,
                (Self::RecDef(l0), Self::RecDef(r0)) => l0 == r0,
                (Self::Rec(l0), Self::Rec(r0)) => l0 == r0,
                _ => false,
            }
        }
    }
}

impl Borrow<Term> for TermId {
    #[inline]
    fn borrow(&self) -> &Term {
        &*self.0
    }
}

impl Equivalent<Code> for TermId {
    #[inline]
    fn equivalent(&self, key: &Code) -> bool {
        self.code() == *key
    }
}

impl Equivalent<Code> for Term {
    #[inline]
    fn equivalent(&self, key: &Code) -> bool {
        self.code() == *key
    }
}

impl Equivalent<TermId> for Code {
    #[inline]
    fn equivalent(&self, key: &TermId) -> bool {
        *self == key.code()
    }
}

impl Equivalent<Term> for Code {
    #[inline]
    fn equivalent(&self, key: &Term) -> bool {
        *self == key.code()
    }
}
