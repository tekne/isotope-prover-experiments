/*!
Typing universes
*/
use super::*;

/// A typing universe
#[derive(Clone)]
pub struct Universe {
    //TODO: polymorphism, etc.
    /// The level of this typing universe
    level: u64,
    /// This universe's type annotation, if any
    ty: Option<TermId>,
    /// This universe's free-variable set
    fv: Filter,
    /// This universe's code
    code: Code,
    /// This universe's flags
    flags: Flags,
}

impl PartialEq for Universe {
    fn eq(&self, other: &Self) -> bool {
        self.code == other.code && self.level == other.level && self.ty == other.ty
    }
}

impl Eq for Universe {}

impl Universe {
    /// Create a universe with a given level and type
    pub fn with_level_ty(level: u64, ty: TermId) -> Result<Universe, Error> {
        if !ty.is_ty() {
            return Err(Error::ExpectedType);
        }
        if Self::is_succ(level, &ty) {
            return Ok(Self::with_level(level));
        }
        let fv = Self::compute_level_fv(level, Some(&ty));
        let code = Self::compute_level_code(level, Some(&ty));
        let flags = Self::compute_flags(level, Some(&ty));
        Ok(Universe {
            level,
            ty: Some(ty),
            fv,
            code,
            flags,
        })
    }

    /// Create a universe with a given level
    pub fn with_level(level: u64) -> Universe {
        let fv = Self::compute_level_fv(level, None);
        let code = Self::compute_level_code(level, None);
        let flags = Self::compute_flags(level, None);
        Universe {
            level,
            ty: None,
            fv,
            code,
            flags,
        }
    }

    /// Get the universe of small types
    #[inline]
    pub fn set() -> Universe {
        Self::with_level(0)
    }

    /// Get the level of a universe
    #[inline]
    pub fn level(&self) -> u64 {
        self.level
    }

    /// Get a successor universe containing this one
    pub fn succ(&self) -> Universe {
        Self::with_level(self.level + 1)
    }

    /// Get whether this universe is an arity with respect to a given sort
    #[inline]
    pub fn is_arity(&self, sort: Option<TermRef>) -> bool {
        if let Some(Term::Universe(sort)) = sort.as_deref() {
            //TODO: subtyping?
            self.level == sort.level
        } else {
            true
        }
    }

    /// Get whether a value is the successor universe of this universe
    #[inline]
    fn is_succ(level: u64, other: &Term) -> bool {
        match other {
            Term::Universe(other) => other.level == level + 1 && other.ty.is_none(),
            _ => false,
        }
    }

    /// Get the sort of this universe as an arity
    #[inline]
    pub fn arity_sort(&self) -> Option<TermRef> {
        None
    }

    /// Get the join of this universe with another
    #[inline]
    pub fn join_universe(
        &self,
        other: &Universe,
        _ctx: &mut impl ConsCtx,
    ) -> Result<Binary<Universe>, Error> {
        if self.level >= other.level && self.ty.is_none() {
            Ok(Binary::Left)
        } else if self.level <= other.level && other.ty.is_none() {
            Ok(Binary::Right)
        } else {
            Ok(Binary::This(Universe::with_level(
                self.level.max(other.level),
            )))
        }
    }

    /// Compute the free variable set of a typing universe with a given level and (optional) annotation
    pub fn compute_level_fv(level: u64, annot: Option<&TermId>) -> Filter {
        let _ = level;
        annot.fv()
    }

    /// Compute the code of a typing universe with a given level and (optional) annotation
    pub fn compute_level_code(level: u64, annot: Option<&TermId>) -> Code {
        let mut hasher = AHasher::new_with_keys(0xfff55762aa68, 0x254a571467);
        level.hash(&mut hasher);
        annot.hash(&mut hasher);
        Code(hasher.finish())
    }

    /// Compute the flags of a typing universe with a given level and (optional) annotation
    pub fn compute_flags(_level: u64, annot: Option<&TermId>) -> Flags {
        let mut flags = Flags::default();
        flags.set(Flags::IS_TYPE, true);
        flags.set(Flags::IS_UNIV, true);
        flags.set(Flags::IS_CLOSED, annot.fv().is_empty());
        flags.set(Flags::TYCK_NIL, annot.is_none()); //TODO: optimize?
        flags
    }
}

self_cons! {
    Universe;
    this, ctx => {
        let ty = this.ty.cons(ctx)?;
        Some(Universe { ty, ..*this })
    }
}

impl Cast<Universe> for Universe {
    fn cast(&self, ty: TermRef, _ctx: &mut impl ConsCtx) -> Result<Option<Universe>, Error> {
        //TODO: fixme, and think about this
        if self.ty.as_ref() == Some(&ty) || (self.ty.is_none() && Self::is_succ(self.level, &ty)) {
            Ok(None)
        } else {
            //TODO: optimize
            Universe::with_level_ty(self.level, ty.into_owned()).map(Some)
        }
    }
}

impl Subst<Universe> for Universe {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Universe>, Error> {
        let ty = self.ty.subst(ctx)?;
        debug_assert!(
            matches!(ty, None | Some(Some(_))),
            "Substituting an `Option` should never yield `Some(None)`"
        );
        if let Some(ty) = ty.flatten() {
            Universe::with_level_ty(self.level, ty).map(Some)
        } else {
            Ok(None)
        }
    }
}

impl Equiv<Universe> for Universe {
    fn term_eq_mut(
        &self,
        other: &Universe,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        if self.level == other.level {
            //TODO: optimize
            self.ty().term_eq_mut(&other.ty(), fail_early, ctx)
        } else {
            Some(false)
        }
    }
}

eq_term!(Universe);

value_impl!(Universe);

impl Value for Universe {
    #[inline]
    fn into_term(self) -> Term {
        Term::Universe(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        if let Some(annot) = &self.ty {
            Some(annot.borrow_id())
        } else {
            self.base_ty()
        }
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.succ().into_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        if let Some(ty) = &self.ty {
            opt_and!(
                fail_early;
                ctx.constrain_mut(ty, &self.succ().into_id(), fail_early),
                ty.tyck_mut(fail_early, ctx)
            )
        } else {
            Some(true)
        }
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn is_universe(&self) -> bool {
        true
    }

    #[inline]
    fn is_ty(&self) -> bool {
        true
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        match other {
            Term::Universe(other) => self.level <= other.level,
            _ => false,
        }
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        true
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.ty.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        if let Some(ty) = &self.ty {
            ty.fv()
        } else {
            Filter::EMPTY
        }
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn join_ty(&self, other: TermRef, ctx: &mut impl ConsCtx) -> Result<Binary<TermId>, Error> {
        match &*other {
            Term::Universe(other) => Ok(self.join_universe(other, ctx)?.map(|u| u.into_id_in(ctx))),
            _ => Err(Error::ExpectedUni),
        }
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        _shift: VarIx,
        _visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        //TODO: hash transitives?
        Ok(())
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then_with(|| self.level.cmp(&other.level))
            .then_with(|| self.ty.cmp(&other.ty))
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, _trans: &mut impl FnMut(&mut H, &TermRef)) {
        //TODO: hash type?
        self.level.hash(hasher);
    }
}

impl Debug for Universe {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut tup = f.debug_tuple("Universe");
        tup.field(&self.level);
        if let Some(ty) = &self.ty {
            tup.field(ty);
        }
        tup.finish()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_set_properties() {
        let s = Universe::set();
        assert!(s.is_ty());
        assert!(s.is_universe());
        let t = s.succ();
        assert!(t.is_ty());
        assert!(t.is_universe());
        assert_ne!(s.code(), t.code());
    }
}
