/*!
Checkable properties of terms
*/

use super::*;

impl TermRef<'_> {
    /// Get the sort of this arity. If this term is not an arity, return an error.
    #[inline]
    pub fn arity_sort_id(&self) -> Result<TermRef, Error> {
        Ok(self.arity_sort()?.unwrap_or(self.borrow_id()))
    }
}

impl Term {
    /// Check whether a type is an arity, optionally for a given sort
    ///
    /// Note this does *not* perform any reduction, and in particular, may return `false` for a term but `true` for it's redex.
    pub fn is_arity(&self, sort: Option<TermRef>) -> bool {
        match self {
            Term::Pi(this) => this.is_arity(sort),
            Term::Universe(this) => this.is_arity(sort),
            _ => false,
        }
    }

    /// Get the sort of this arity. If this term is not an arity, return an error. If this term is it's own arity, return `None`.
    ///
    /// Note this does *not* perform any reduction, and in particular, may return an error for a term but a valid sort for it's redex.
    /// The sort of a redex may also be a subtype of the sort of the unreduced term, if both are arities.
    ///
    /// If `t.arity_sort() == Ok(sort)`, then `t.is_arity(Some(sort))` should return `true`.
    pub fn arity_sort(&self) -> Result<Option<TermRef>, Error> {
        match self {
            Term::Pi(this) => this.arity_sort().map(Some),
            Term::Universe(this) => Ok(this.arity_sort()),
            _ => Err(Error::ExpectedArity),
        }
    }

    /// Check whether a term is a type of constructor for a given inductive family, given it's type and sort.
    ///
    /// Note this does *not* perform any reduction, and in particular, may return `false` for a term but `true` for it's redex.
    /// The wrong result may be returned if `ty` is not an arity, or if `sort` is not the sort of `ty`.
    pub fn is_type_of_constructor(
        &self,
        mut var: VarIx,
        family: VarIx,
        ty: TermRef,
        mut sort: TermRef,
    ) -> Result<bool, Error> {
        if !self.is_ty() {
            return Ok(false);
        }
        let mut this = self;
        let mut shift = 0;
        loop {
            match this {
                Term::App(app) => {
                    if app.right().has_dep(var) {
                        return Ok(false);
                    }
                    this = app.left();
                }
                Term::Pi(pi) => {
                    if !pi.param_ty().ty().is_subty(&*sort) {
                        return Ok(false);
                    }
                    var = var.checked_add(1).ok_or(Error::VarIxOverflow)?;
                    shift += 1;
                    //TODO: pass in context?
                    sort = sort.shifted(1, 0, &mut ()).unwrap();
                    this = pi.result();
                }
                _ => break,
            }
        }
        let result = match this {
            Term::IndFamily(this) => match &**this.def() {
                Term::Var(def) => {
                    if var == def.ix() && family == this.ix() {
                        this.ty().is_subty(&*ty.shifted(shift, 0, &mut ())?)
                    } else {
                        false
                    }
                }
                _ => false,
            },
            _ => false,
        };
        Ok(result)
    }

    /// Check whether a term satisfies the strict positivity condition for a given set of inductive families.
    pub fn satisfies_positivity_condition(&self, mut var: VarIx) -> Result<bool, Error> {
        let mut this = self;
        loop {
            match this {
                Term::App(app) => {
                    if app.right().has_dep(var) {
                        return Ok(false);
                    }
                    this = app.left();
                }
                Term::Pi(pi) => {
                    if !pi.param_ty().occurs_strictly_positively(var)? {
                        return Ok(false);
                    }
                    var = var
                        .checked_add(1)
                        .ok_or(Error::VarIxOverflow)?;
                    this = pi.result();
                }
                _ => break,
            }
        }
        //TODO: think about this...
        let result = match this {
            Term::IndFamily(this) => match &**this.def() {
                Term::Var(this) => this.ix() == var,
                _ => false,
            },
            _ => false,
        };
        Ok(result)
    }

    /// Check whether a set of inductively defined families occur strictly positively in a term
    ///
    /// Note this does *not* perform any reduction, and in particular, may return `false` for a term but `true` for it's redex.
    pub fn occurs_strictly_positively(&self, mut var: VarIx) -> Result<bool, Error> {
        let mut this = self;
        loop {
            if matches!(this.fv().contains(var), Some(false)) {
                return Ok(true);
            }
            match this {
                Term::App(app) => {
                    if app.right().has_dep(var) {
                        return Ok(false);
                    }
                    this = app.left();
                }
                Term::Pi(pi) => {
                    if pi.param_ty().has_dep(var) {
                        return Ok(false);
                    }
                    var = var
                        .checked_add(1)
                        .ok_or(Error::VarIxOverflow)?;
                    this = pi.result();
                }
                _ => break,
            }
        }
        let result = if this.has_dep(var) {
            match this {
                Term::IndFamily(this) => {
                    match &**this.def() {
                        Term::Var(def) => {
                            //TODO: think about this. In particular, if the index is *not* the variable, it means the variable must show up in the annotation, right? Is that an error?
                            def.ix() == var
                        }
                        Term::IndDef(def) => {
                            if def.len() == 1 {
                                var = var.checked_add(1).ok_or(Error::VarIxOverflow)?;
                                for con in def[0].cons.iter() {
                                    if !con.satisfies_nested_positivity_condition(var)? {
                                        return Ok(false);
                                    }
                                }
                                true
                            } else {
                                debug_assert_ne!(def.len(), 0);
                                // We do not support nested positivity for mutually inductive types, yet!
                                false
                            }
                        }
                        _ => false,
                    }
                }
                _ => false,
            }
        } else {
            true
        };
        Ok(result)
    }

    /// Check whether a constructor for an inductive definition satisfies the nested positivity condition for a set of inductively defined families
    ///
    /// Note this does *not* perform any reduction, and in particular, may return `false` for a term but `true` for it's redex.
    pub fn satisfies_nested_positivity_condition(&self, mut var: VarIx) -> Result<bool, Error> {
        let mut this = self;
        loop {
            if matches!(this.fv().contains(var), Some(false)) {
                return Ok(true);
            }
            match this {
                Term::App(app) => {
                    if app.right().has_dep(var) {
                        return Ok(false);
                    }
                    this = app.left();
                }
                Term::Pi(pi) => {
                    if !pi.param_ty().occurs_strictly_positively(var)? {
                        return Ok(false);
                    }
                    var = var
                        .checked_add(1)
                        .ok_or(Error::VarIxOverflow)?;
                    this = pi.result();
                }
                _ => break,
            }
        }
        Ok(matches!(this, Term::IndFamily(_)))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    //TODO: show box is type of constructor for type

    //TODO: show box is not type of constructor for set

    #[test]
    fn negative_type_is_not_strictly_positive() -> Result<(), Error> {
        let mut ctx = HashCtx::default();
        let set = TermId::set_in(&mut ctx);
        let def_var = set.var_fam_in(0, 0, &mut ctx)?;
        let unary = def_var.simple_in(def_var.clone(), &mut ctx)?;
        assert_eq!(unary.occurs_strictly_positively(0), Ok(false));
        assert_eq!(unary.satisfies_positivity_condition(0), Ok(true));
        assert_eq!(
            unary.is_type_of_constructor(0, 0, set.borrow_id(), set.borrow_id()),
            Ok(true)
        );
        for i in 1..5 {
            assert_eq!(unary.occurs_strictly_positively(i), Ok(true));
            assert_eq!(
                unary.is_type_of_constructor(i, 0, set.borrow_id(), set.borrow_id()),
                Ok(false)
            );
            for j in 0..5 {
                assert_eq!(
                    unary.is_type_of_constructor(j, i, set.borrow_id(), set.borrow_id()),
                    Ok(false)
                );
            }
        }
        let lam = unary.simple_in(def_var.clone(), &mut ctx)?;
        assert_eq!(lam.occurs_strictly_positively(0), Ok(false));
        assert_eq!(lam.satisfies_positivity_condition(0), Ok(false));
        assert_eq!(
            lam.is_type_of_constructor(0, 0, set.borrow_id(), set.borrow_id()),
            Ok(true)
        );
        for i in 1..5 {
            assert_eq!(lam.occurs_strictly_positively(i), Ok(true));
            assert_eq!(
                lam.is_type_of_constructor(i, 0, set.borrow_id(), set.borrow_id()),
                Ok(false)
            );
            for j in 0..5 {
                assert_eq!(
                    lam.is_type_of_constructor(j, i, set.borrow_id(), set.borrow_id()),
                    Ok(false)
                );
            }
        }
        Ok(())
    }
}
