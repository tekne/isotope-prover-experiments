/*!
Terms in `isotope`'s underlying dependent type system
*/
use elysees::ArcRef;

use crate::*;

mod app;
mod case;
mod inductive;
mod lambda;
mod properties;
mod rec;
mod universe;
mod value;
mod var;

pub use app::*;
pub use case::*;
pub use inductive::*;
pub use lambda::*;
pub use properties::*;
pub use rec::*;
pub use universe::*;
pub use value::*;
pub use var::*;
pub use constructors::*;

mod constructors;
mod normalization;
mod term_impl;

/// A term in `isotope`'s term language
#[derive(Clone)]
pub struct TermRef<'a>(ArcRef<'a, Term>);

impl<'a> TermRef<'a> {
    /// Create a new, unique `TermId` directly from a `Term`
    #[inline]
    pub fn new_unique(term: Term) -> TermId {
        TermRef(ArcRef::new(term))
    }

    /// Try to borrow a `TermRef` as a `Term`
    #[inline]
    pub fn try_borrow(&self) -> Either<&'a Term, &Term> {
        if !ArcRef::is_owned(&self.0) {
            //TODO: go add a proper method to `elysees`...
            Either::Left(ArcRef::try_into_arc(self.0.clone()).unwrap_err().get())
        } else {
            Either::Right(&*self.0)
        }
    }

    /// Borrow a `TermRef`
    #[inline]
    pub fn borrow_id(&'a self) -> TermRef<'a> {
        TermRef(ArcRef::into_borrow(&self.0))
    }

    /// Convert a `TermRef` into an owned `TermId`
    #[inline]
    pub fn into_owned(self) -> TermId {
        TermRef(ArcRef::into_owned(self.0))
    }

    /// Clone a `TermRef` into an owned `TermId`
    #[inline]
    pub fn clone_into_owned(&self) -> TermId {
        TermRef(ArcRef::clone_into_owned(&self.0))
    }

    /// Create a shallow-consed `TermId` from a `Term`
    #[inline]
    pub fn shallow_cons(term: Term, ctx: &mut impl ConsCtx) -> TermId {
        if let Some(id) = ctx.try_cons(&term) {
            id.clone()
        } else {
            let id = TermId::new_unique(term);
            ctx.shallow_cons(id.borrow_id()).unwrap_or(id)
        }
    }

    /// Convert a `TermId` into a pointer to a `Term`.
    #[inline]
    pub fn as_ptr(&self) -> *const Term {
        &*self.0
    }

    /// Check a `TermId` for pointer-equality with a `Term`
    #[inline]
    pub fn ptr_eq(&self, term: &Term) -> bool {
        ArcRef::as_ptr(&self.0) == term
    }

    /// Get the root of this term
    #[inline]
    pub fn get_root(&self) -> &TermRef<'a> {
        self.app_root().unwrap_or(self)
    }

    /// Get the fully applied root of this term, if any
    #[inline]
    pub fn get_base_root(&self) -> Result<&TermRef<'a>, Error> {
        Ok(self.base_root()?.unwrap_or(self))
    }

    /// Swap the root target of this term with another, with a given shift
    #[inline]
    pub fn swap_root(&self, ty: TermId, shift: SVarIx) -> Result<TermId, Error> {
        self.swap_root_in(ty, shift, &mut ())
    }

    /// Swap the root target of this term with another, with a given shift
    #[inline]
    pub fn swap_root_in(
        &self,
        ty: TermId,
        shift: SVarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        match &**self {
            Term::Pi(this) => this.swap_root(ty, shift, ctx),
            this => this.shifted(shift, 0, ctx),
        }
    }

    /// Iterate over the parameters of this term
    #[inline]
    pub fn params(&self) -> Params {
        Params(self.borrow_id())
    }

    /// Get the arity of this term
    #[inline]
    pub fn arity(&self) -> usize {
        self.params().count()
    }

    /// Iterate over the arguments of this term
    #[inline]
    pub fn args(&self) -> Args {
        Args(self.borrow_id())
    }

    /// Visit all dependencies of this term, including this term itself
    #[inline]
    pub fn visit_transitive_deps_rec<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef<'_>) -> Result<bool, E>,
    ) -> Result<(), E> {
        if visitor(false, shift, self.borrow_id())? {
            self.0.visit_transitive_deps(shift, visitor)?;
        }
        Ok(())
    }

    /// Get the number of constructors for this family
    #[inline]
    pub fn no_cons(&self) -> Result<VarIx, Error> {
        self.as_ind_fam()?.no_cons()
    }

    /// Get the number of parameters for a constructor for this family
    #[inline]
    pub fn no_params(&self, ix: VarIx) -> Result<VarIx, Error> {
        self.as_ind_fam()?.no_params(ix)
    }
}

/// A term in `isotope`'s term language, which may be borrowed or owned
pub type TermId = TermRef<'static>;

/// An enumeration of possible terms in `isotope`'s term language
//TODO: separate terms and expressions? Refactor, regardless.
//TODO: vectored applications?
#[derive(Clone, Eq)]
pub enum Term {
    // === Basic dependent type theory ===
    /// A variable
    Var(Var),
    /// A lambda function
    Lambda(Lambda),
    /// A function application
    App(App),
    /// A pi type
    Pi(Pi),
    /// A typing universe
    Universe(Universe),

    //TODO: symbols

    //TODO: universe levels

    // === Inductive types ===
    /// An inductive definition
    IndDef(IndDef),
    /// An inductive family
    IndFamily(IndFamily),
    /// An inductive constructor
    IndCons(IndCons),
    /// A case statement
    Case(Case),

    //TODO: natural numbers optimization

    //TODO: coinduction?

    // === Recursive definitions ===
    /// A set of mutually-recursive definitions
    RecDef(RecDef),
    /// A (mutually) recursively-defined term
    Rec(Rec),

    //TODO: metavariables?
}

impl Term {
    /// Get this term as a variable, if possible
    #[inline]
    pub fn as_var(&self) -> Result<&Var, Error> {
        match self {
            Term::Var(this) => Ok(this),
            _ => Err(Error::ExpectedVar),
        }
    }

    /// Get this term as a typing universe, if possible
    #[inline]
    pub fn as_uni(&self) -> Result<&Universe, Error> {
        match self {
            Term::Universe(this) => Ok(this),
            _ => Err(Error::ExpectedUni),
        }
    }

    /// Get this term as a pi type, if possible
    #[inline]
    pub fn as_pi(&self) -> Result<&Pi, Error> {
        match self {
            Term::Pi(this) => Ok(this),
            _ => Err(Error::ExpectedPi),
        }
    }

    /// Get this term as lambda function, if possible
    #[inline]
    pub fn as_lambda(&self) -> Result<&Lambda, Error> {
        match self {
            Term::Lambda(this) => Ok(this),
            _ => Err(Error::ExpectedFn),
        }
    }

    /// Get this term as an inductive definition, if possible
    #[inline]
    pub fn as_ind_def(&self) -> Result<&IndDef, Error> {
        match self {
            Term::IndDef(this) => Ok(this),
            _ => Err(Error::ExpectedIndDef),
        }
    }

    /// Get this term as an inductive family, if possible
    #[inline]
    pub fn as_ind_fam(&self) -> Result<&IndFamily, Error> {
        match self {
            Term::IndFamily(this) => Ok(this),
            _ => Err(Error::ExpectedIndFam),
        }
    }

    /// Get this term as an inductive constructor, if possible
    #[inline]
    pub fn as_ind_cons(&self) -> Result<&IndCons, Error> {
        match self {
            Term::IndCons(this) => Ok(this),
            _ => Err(Error::ExpectedIndCons),
        }
    }

    /// Get this term as a mutually recursive definition, if possible
    #[inline]
    pub fn as_rec_def(&self) -> Result<&RecDef, Error> {
        match self {
            Term::RecDef(this) => Ok(this),
            _ => Err(Error::ExpectedRecDef),
        }
    }

    /// Get this term as a recursively defined term, if possible
    #[inline]
    pub fn as_rec(&self) -> Result<&Rec, Error> {
        match self {
            Term::Rec(this) => Ok(this),
            _ => Err(Error::ExpectedRec),
        }
    }

    /// Get this term as a binary application, if possible
    #[inline]
    pub fn as_bin_app(&self) -> Result<(TermRef, TermRef), Error> {
        match self {
            Term::App(this) => Ok((this.left().borrow_id(), this.right().borrow_id())),
            _ => Err(Error::ExpectedVar),
        }
    }

    /// Get this term's decreasing arguments, if any
    #[inline]
    pub fn dec_args(&self) -> Result<Option<&[usize]>, Error> {
        Ok(self.as_rec()?.dec_args())
    }

    /// Construct the `n`th constructor for this family
    #[inline]
    pub fn make_con(&self, n: VarIx, ctx: &mut impl ConsCtx) -> Result<IndCons, Error> {
        self.as_ind_fam()?.make_con(n, ctx)
    }

    /// Get the `n`th branch type for this inductive family, given a target family
    #[inline]
    pub fn branch_ty(
        &self,
        n: VarIx,
        target_family: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        self.as_ind_fam()?.branch_ty(n, target_family, ctx)
    }

    /// Visit the root arguments to this term
    #[inline]
    pub fn visit_root_args<E>(
        &self,
        mut visitor: impl FnMut(&TermId) -> Result<(), E>,
    ) -> Result<(), E> {
        let mut curr = self;
        while let Term::App(app) = curr {
            visitor(app.right())?;
            curr = app.left();
        }
        Ok(())
    }

    /// Push the root arguments to this term
    #[inline]
    pub fn push_root_args(&self, args: &mut impl ArgVec) -> Result<(), Error> {
        self.visit_root_args(|arg| args.push_arg(arg.clone()))
    }
}

/// Return the same expression for every variant of `Term`
#[macro_export]
macro_rules! for_term {
    ($t:expr; $i:ident => $e:expr $(,$p:pat => $r:expr)*) => {
        {
            #[allow(unreachable_patterns)]
            match $t {
                $($p => $r,)*
                Term::Var($i) => $e,
                Term::Lambda($i) => $e,
                Term::App($i) => $e,
                Term::Pi($i) => $e,
                Term::Universe($i) => $e,
                Term::IndDef($i) => $e,
                Term::IndFamily($i) => $e,
                Term::IndCons($i) => $e,
                Term::Case($i) => $e,
                Term::RecDef($i) => $e,
                Term::Rec($i) => $e,
            }
        }
    };
}

/// Return the result of a binary operation on elements of `Term` having the same type
#[macro_export]
macro_rules! for_term_diag {
    ($t:expr; ($l:ident, $r:ident) => $e:expr $(,$p:pat => $pe:expr)*) => {
        {
            #[allow(unreachable_patterns)]
            match $t {
                (Term::Var($l), Term::Var($r)) => $e,
                (Term::Lambda($l), Term::Lambda($r)) => $e,
                (Term::App($l), Term::App($r)) => $e,
                (Term::Pi($l), Term::Pi($r)) => $e,
                (Term::Universe($l), Term::Universe($r)) => $e,
                (Term::IndDef($l), Term::IndDef($r)) => $e,
                (Term::IndFamily($l), Term::IndFamily($r)) => $e,
                (Term::IndCons($l), Term::IndCons($r)) => $e,
                (Term::Case($l), Term::Case($r)) => $e,
                (Term::RecDef($l), Term::RecDef($r)) => $e,
                (Term::Rec($l), Term::Rec($r)) => $e,
                $($p => $pe,)*
            }
        }
    };
}
